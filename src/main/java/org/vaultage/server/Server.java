package org.vaultage.server;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.ConnectionHandler;
import org.vaultage.server.comm.auth.Authenticator;
import org.vaultage.server.comm.auth.UserManager;
import org.vaultage.server.comm.io.CommandResponseHandler;
import org.vaultage.server.comm.io.IOutputHandler;
import org.vaultage.server.comm.io.UserOutputHandler;
import org.vaultage.server.core.Delegator;
import org.vaultage.server.core.sync.SyncManager;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.SessionManager;
import org.vaultage.server.database.readers.DatabaseReaderProvider;
import org.vaultage.server.database.writers.DatabaseWriterProvider;

import ch.marcsladek.commons.concurrent.Startable;
import ch.marcsladek.commons.concurrent.Startup;
import ch.marcsladek.commons.io.ConsoleReader;
import ch.marcsladek.commons.util.config.IllegalPropertyTypeException;

public class Server {

  private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

  public static final Path USER_HOME = Paths.get(System.getProperty("user.home"));
  public static final Path DIR = USER_HOME.resolve(".vaultage-server");

  private static UserManager userManager;
  private static Startable startable;

  public static void main(String[] args) throws Exception {

    ServerConfig config = new ServerConfig();

    SessionManager sessionManager = setupDatabase(config);

    ConsoleReader consoleReader = getConsoleReader();
    ConnectionHandler connHandler = new ConnectionHandler(config.getInt(ServerConfig.PORT),
        config.getInt(ServerConfig.CLIENT_TIMEOUT));
    userManager = connHandler.getUserManager();
    Authenticator authenticator = userManager.getAuthenticator(config
        .getString(ServerConfig.DATABASE_SECRET));
    IOutputHandler outputHandler = new UserOutputHandler(userManager);
    SyncManager syncManager = new SyncManager(
        config.getInt(ServerConfig.SYNC_MANAGER_ALLOWED_TIME), TimeUnit.SECONDS, outputHandler);
    Delegator delegator = new Delegator(authenticator, syncManager);
    CommandResponseHandler requestResponseHandler = new CommandResponseHandler(userManager,
        sessionManager, delegator, outputHandler);
    userManager.setCallable(requestResponseHandler);

    startable = new Startup(DatabaseAccess.getDatabaseAccessStartable(), connHandler, userManager,
        syncManager, consoleReader);
    startable.start();
    LOGGER.info("Server started. Listening to port " + config.getInt(ServerConfig.PORT));
  }

  private static SessionManager setupDatabase(ServerConfig config) throws DatabaseException,
      IllegalPropertyTypeException {
    SessionManager sessionManager = new SessionManager(config.getPath(ServerConfig.DATABASE_PATH)
        .toString(), config.getString(ServerConfig.DATABASE_USER),
        config.getString(ServerConfig.DATABASE_PASSWORD));
    DatabaseAccess.initialise(sessionManager, new DatabaseReaderProvider(),
        new DatabaseWriterProvider(), config.getString(ServerConfig.ADMIN_PASSWORD));
    return sessionManager;
  }

  private static ConsoleReader getConsoleReader() {
    return new ConsoleReader() {

      @Override
      protected void process(String line) {
        line = line.toLowerCase();
        if (line.equals("quit")) {
          startable.shutdown();
        } else if (line.equals("list")) {
          System.out.println(userManager.list());
        } else if (line.startsWith("kill")) {
          try {
            String identifier = line.substring(line.lastIndexOf(" ") + 1, line.length());
            userManager.removeClient(identifier);
            System.out.println(identifier + " removed");
          } catch (Exception exc) {
            exc.printStackTrace();
          }
        } else if (line.startsWith("create")) {
          try {
            String name = line.substring(line.indexOf(" ") + 1, line.lastIndexOf(" "));
            String password = line.substring(line.lastIndexOf(" ") + 1, line.length());
            // DatabaseUtils.createUser(name, password); TODO
            System.out.println("Created user '" + name + "' with pw '" + password + "'");
          } catch (Exception exc) {
            exc.printStackTrace();
          }
        } else {
          System.out.println("Type 'quit' for closing the client");
        }
      }
    };
  }

}
