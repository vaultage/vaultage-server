package org.vaultage.server.core.sync;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.ClientIdentity;
import org.vaultage.server.core.diff.Diff;
import org.vaultage.server.core.diff.DiffType;
import org.vaultage.server.core.diff.FSObjectDiffBuilder;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.DatabaseObjectConverter;
import org.vaultage.server.database.classes.Meta;
import org.vaultage.server.database.classes.MetaContainer;
import org.vaultage.server.database.writers.MetaContainerWriter;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.meta.IMeta;
import org.vaultage.shared.fs.meta.MetaFile;

public class SyncContainerBuilder {

  private static final Logger LOGGER = LoggerFactory.getLogger(SyncContainerBuilder.class);

  private final ClientIdentity identity;

  private Map<Meta, MetaFile> requestMap;
  private Map<Meta, MetaFile> sendMap;
  private Map<String, Meta> metaMap;

  SyncContainerBuilder(ClientIdentity identity) {
    this.identity = identity;
  }

  SyncContainer build(FSObject<IMeta> fsObj) throws SyncContainerBuildException {
    try {
      requestMap = new HashMap<>();
      sendMap = new HashMap<>();
      FSObject<IMeta> lastFSObj = getLastFSObjectFromDatabase(fsObj.getRoot());
      MetaContainer container = DatabaseObjectConverter.convertWithDBCheck(
          DatabaseAccess.getSession(identity), fsObj, null, new Date());
      metaMap = writeContainerToDatabase(container);
      FSObject<Diff> fsObjDiff = new FSObjectDiffBuilder().build(lastFSObj, fsObj);
      fillUpMaps(fsObjDiff.getUnmodifiableMap().values());

      return new SyncContainer(container, requestMap, sendMap);
    } catch (Exception exc) {
      throw new SyncContainerBuildException("Failed to build SyncContainer for identity '"
          + identity + "' and fsObj '" + fsObj + "'", exc);
    }
  }

  private FSObject<IMeta> getLastFSObjectFromDatabase(String root) throws DatabaseException {
    FSObject<IMeta> lastFSObj;
    MetaContainer container = DatabaseAccess.getReaderProvider().getMetaContainerReader()
        .getLastMetaContainer(DatabaseAccess.getSession(identity), identity.getUserId());
    if (container != null) {
      lastFSObj = DatabaseObjectConverter.convert(container);
      LOGGER.trace("Retrieved last FSObject '" + lastFSObj + "'");
    } else {
      lastFSObj = new FSObject<>(root);
      LOGGER.debug("Unable to retrieve last FSObject, creating new '" + lastFSObj + "'");
    }
    return lastFSObj;
  }

  private Map<String, Meta> writeContainerToDatabase(MetaContainer container)
      throws DatabaseException {
    MetaContainerWriter writer = DatabaseAccess.getWriterProvider().getMetaContainerWriter();
    writer.writeMetaContainerForUser(DatabaseAccess.getSession(identity), identity.getUserId(),
        container);
    Map<String, Meta> metaMap = new HashMap<>();
    for (Meta meta : container.getElements()) {
      if (meta.getIsFile()) {
        metaMap.put(meta.getPath(), meta);
      }
    }
    return metaMap;
  }

  private void fillUpMaps(Collection<Diff> diffs) {
    for (Diff diff : diffs) {
      checkSendFile(diff);
      checkRequestFile(diff);
    }
  }

  // TODO simple single client implementation
  private void checkSendFile(Diff diff) {
  }

  // TODO simple single client implementation
  private void checkRequestFile(Diff diff) {
    IMeta meta = diff.getMeta();
    if ((meta != null) && !meta.isFolder()) {
      if ((diff.getDiffType() == DiffType.CREATED) || (diff.getDiffType() == DiffType.MODIFIED)) {
        Meta dbMeta = Objects.requireNonNull(metaMap.get(meta.getPath()));
        if (dbMeta.getData() == null) {
          requestMap.put(dbMeta, (MetaFile) meta);
          LOGGER.trace("Added to requestMap '" + meta + "'");
        } else {
          LOGGER.trace("Not added to requestMap '" + meta + "'");
        }
      }
    }
  }

}
