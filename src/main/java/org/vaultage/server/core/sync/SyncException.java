package org.vaultage.server.core.sync;

public class SyncException extends Exception {

  private static final long serialVersionUID = 201304111312L;

  public SyncException() {
    super();
  }

  public SyncException(String msg) {
    super(msg);
  }

  public SyncException(String msg, Throwable cause) {
    super(msg, cause);
  }

}

class AlreadyEnrolledException extends SyncException {

  private static final long serialVersionUID = 201311201540L;

  public AlreadyEnrolledException() {
    super();
  }

  public AlreadyEnrolledException(String msg) {
    super(msg);
  }

  public AlreadyEnrolledException(String msg, Throwable cause) {
    super(msg, cause);
  }

}

class NotEnrolledException extends SyncException {

  private static final long serialVersionUID = 201311201454L;

  public NotEnrolledException() {
    super();
  }

  public NotEnrolledException(String msg) {
    super(msg);
  }

  public NotEnrolledException(String msg, Throwable cause) {
    super(msg, cause);
  }

}

class SyncContainerBuildException extends SyncException {

  private static final long serialVersionUID = 201311201454L;

  public SyncContainerBuildException() {
    super();
  }

  public SyncContainerBuildException(String msg) {
    super(msg);
  }

  public SyncContainerBuildException(String msg, Throwable cause) {
    super(msg, cause);
  }

}
