package org.vaultage.server.core.sync;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.ClientIdentity;
import org.vaultage.server.comm.io.IOutputHandler;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.meta.IMeta;

import ch.marcsladek.commons.concurrent.TimedManager;

public class SyncManager extends TimedManager<ClientIdentity, SyncSupervisor> {

  private static final Logger LOGGER = LoggerFactory.getLogger(SyncManager.class);

  private final IOutputHandler outputHandler;

  public SyncManager(long allowedTime, TimeUnit unit, IOutputHandler outputHandler) {
    super(allowedTime, unit);
    this.outputHandler = Objects.requireNonNull(outputHandler);
  }

  public void enroll(ClientIdentity identity, FSObject<IMeta> fsObj) throws SyncException {
    SyncSupervisor supervisor = new SyncSupervisor(identity, outputHandler);
    if (super.enroll(supervisor)) {
      LOGGER.debug("Successfully enrolled '" + supervisor + "'");
      supervisor.initialise(fsObj, new DisenrollToken());
    } else {
      throw new AlreadyEnrolledException("SyncSupervisor already enrolled for identity '"
          + identity + "'");
    }
  }

  public SyncSupervisor getSyncSupervisor(ClientIdentity identity) throws SyncException {
    Pair<SyncSupervisor, Long> pair = enrolleeMap.get(identity);
    if (pair != null) {
      return pair.getKey();
    } else {
      throw new NotEnrolledException("No SyncSupervisor enrolled for '" + identity + "'");
    }
  }

  class DisenrollToken {

    private boolean usable;

    private DisenrollToken() {
      usable = true;
    }

    boolean use(SyncSupervisor disenrolee) {
      if (usable) {
        usable = false;
        LOGGER.debug("DisenrollToken used for '" + disenrolee + "'");
        return SyncManager.this.disenroll(disenrolee);
      } else {
        throw new IllegalStateException("DisenrollToken has already been used");
      }
    }

  }

}
