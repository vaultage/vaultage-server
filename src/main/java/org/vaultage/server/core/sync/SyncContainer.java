package org.vaultage.server.core.sync;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.vaultage.server.database.classes.Meta;
import org.vaultage.server.database.classes.MetaContainer;
import org.vaultage.shared.fs.FSObjectBuildException;
import org.vaultage.shared.fs.meta.MetaFile;

class SyncContainer {

  private final MetaContainer container;
  private final MapIter requestFiles;
  private final MapIter sendFiles;

  SyncContainer(MetaContainer container, Map<Meta, MetaFile> requestMap, Map<Meta, MetaFile> sendMap)
      throws FSObjectBuildException {
    this.container = container;
    requestFiles = new MapIter(requestMap);
    sendFiles = new MapIter(sendMap);
  }

  MetaContainer getMetaContainer() {
    return container;
  }

  boolean hasNextRequest() {
    return requestFiles.hasNext();
  }

  Entry<Meta, MetaFile> getNextRequest() {
    return requestFiles.next();
  }

  Entry<Meta, MetaFile> getCurrentRequest() {
    return requestFiles.current;
  }

  boolean hasNextSend() {
    return requestFiles.hasNext();
  }

  Entry<Meta, MetaFile> getNextSend() {
    return requestFiles.next();
  }

  Entry<Meta, MetaFile> getCurrentSend() {
    return requestFiles.current;
  }

  boolean isDone() {
    return !hasNextRequest() && !hasNextSend();
  }

  @Override
  public String toString() {
    return "SyncFileContainer [requestFiles=" + requestFiles.map + ", sendFiles=" + sendFiles.map
        + "]";
  }

  private class MapIter {

    private final Map<Meta, MetaFile> map;
    private final Iterator<Entry<Meta, MetaFile>> iter;
    Entry<Meta, MetaFile> current;

    MapIter(Map<Meta, MetaFile> map) {
      this.map = map;
      this.iter = map.entrySet().iterator();
    }

    boolean hasNext() {
      if (!iter.hasNext()) {
        current = null;
      }
      return iter.hasNext();
    }

    Entry<Meta, MetaFile> next() {
      current = iter.next();
      return current;
    }

  }

}
