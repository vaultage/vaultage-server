package org.vaultage.server.core.sync;

import java.util.Map.Entry;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.ClientIdentity;
import org.vaultage.server.comm.io.IOutputHandler;
import org.vaultage.server.core.sync.SyncManager.DisenrollToken;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.classes.Meta;
import org.vaultage.server.database.classes.MetaContainer;
import org.vaultage.server.database.writers.DataWriter;
import org.vaultage.server.database.writers.MetaContainerWriter;
import org.vaultage.shared.comm.command.ClientCommandType;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.FileData;
import org.vaultage.shared.fs.meta.IMeta;
import org.vaultage.shared.fs.meta.MetaFile;

import ch.marcsladek.commons.concurrent.Startable;
import ch.marcsladek.commons.concurrent.TimeManageable;
import ch.marcsladek.commons.concurrent.TimedManager;

public class SyncSupervisor extends Thread implements TimeManageable<ClientIdentity>, Startable {

  private static final Logger LOGGER = LoggerFactory.getLogger(SyncSupervisor.class);

  private final ClientIdentity identity;
  private final IOutputHandler outputHandler;

  private DisenrollToken token;
  private SyncContainer syncContainer;

  private volatile boolean started;
  private volatile boolean running;
  private final Object lock;

  SyncSupervisor(ClientIdentity identity, IOutputHandler outputHandler)
      throws SyncContainerBuildException {
    this.identity = identity;
    this.outputHandler = outputHandler;
    started = running = false;
    lock = new Object();
  }

  void initialise(FSObject<IMeta> fsObj, DisenrollToken token) throws SyncContainerBuildException {
    if (syncContainer == null) {
      syncContainer = new SyncContainerBuilder(identity).build(fsObj);
      this.token = token;
      this.start();
      LOGGER.trace("Initialised and started '" + this + "'");
    } else {
      throw new IllegalStateException("Already initialised");
    }
  }

  @Override
  public ClientIdentity getIdentifier() {
    return identity;
  }

  @Override
  public void notifyDisenroll(
      TimedManager<ClientIdentity, ? extends TimeManageable<ClientIdentity>> timedManager) {
    shutdown();
  }

  public boolean wasSuccessful() {
    return syncContainer.isDone()
        && syncContainer.getMetaContainer().getStatus() == MetaContainer.STATUS_OK;
  }

  @Override
  public void run() {
    started = running = true;
    while (running && syncContainer.hasNextRequest()) {
      MetaFile metaFile = syncContainer.getNextRequest().getValue();
      synchronized (lock) {
        outputHandler.handle(identity.getHost(), ClientCommandType.FileRequestCommand, metaFile);
        try {
          LOGGER.trace("Waiting for FileSendCommand...: " + this);
          lock.wait();
        } catch (InterruptedException exc) {
          Thread.currentThread().interrupt();
        }
      }
    }

    // TODO send

    running = false;
    useToken();
    outputHandler.handle(identity.getHost(), ClientCommandType.SyncEndCommand, wasSuccessful());
    LOGGER.trace("SyncSupervisor ended: " + this);
  }

  @Override
  public boolean isRunning() {
    return running;
  }

  @Override
  public void shutdown() {
    running = false;
    synchronized (lock) {
      lock.notify();
    }
    this.interrupt();
  }

  @Override
  public boolean isTerminated() {
    return started && !running;
  }

  public void requested(FileData fileData) throws SyncException {
    Entry<Meta, MetaFile> current = syncContainer.getCurrentRequest();
    try {
      if (validMetaFiles(fileData.getMetaFile(), current != null ? current.getValue() : null)) {
        try {
          writeData(current.getKey(), fileData);
          if (syncContainer.isDone()) {
            updateStatus(syncContainer.getMetaContainer(), MetaContainer.STATUS_OK);
          }
        } catch (DatabaseException exc) {
          throw new SyncException("Error accessing database during sync", exc);
        }
      } else {
        throw new SyncException("Invalid file received '" + fileData.getMetaFile()
            + "' instead of '" + current.getValue() + "'");
      }
    } catch (SyncException exc) {
      useToken();
      try {
        updateStatus(syncContainer.getMetaContainer(), MetaContainer.STATUS_ERROR);
      } catch (DatabaseException dbExc) {
        LOGGER.error("Error while updating container to error status", dbExc);
      }
      throw exc;
    } finally {
      notifyOnLock();
    }
  }

  private void writeData(Meta meta, FileData fileData) throws DatabaseException {
    DataWriter writer = DatabaseAccess.getWriterProvider().getDataWriter();
    writer.writeData(DatabaseAccess.getSession(identity), meta, fileData.getMetaFile().getHash(),
        fileData.getData());
  }

  private void updateStatus(MetaContainer container, Integer status) throws DatabaseException {
    container.setStatus(status);
    MetaContainerWriter writer = DatabaseAccess.getWriterProvider().getMetaContainerWriter();
    writer.update(DatabaseAccess.getSession(identity), container);
    LOGGER.trace("Updated container status for '" + container + "'");
  }

  // TODO has to be called if file was successfully transfered to FS on client (hash check)
  public void sent(MetaFile file) throws SyncException {
    throw new SyncException("Not implemented");
  }

  private boolean validMetaFiles(MetaFile file1, MetaFile file2) {
    return file1 != null && file1.equals(file2);
  }

  private void notifyOnLock() {
    synchronized (lock) {
      LOGGER.trace("Notified: " + this);
      lock.notify();
    }
  }

  private void useToken() {
    try {
      if (token != null && token.use(this)) {
        LOGGER.trace("Successfully disenrolled '" + this + "'");
      } else {
        LOGGER.error("Disenroll failed for '" + this + "'");
      }
    } catch (IllegalStateException exc) {
      // silent catch, multiple calls
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(identity);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof SyncSupervisor) {
      SyncSupervisor other = (SyncSupervisor) obj;
      return Objects.equals(identity, other.identity);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "SyncSupervisor [identity=" + identity + ", syncFileContainer=" + syncContainer + "]";
  }

}
