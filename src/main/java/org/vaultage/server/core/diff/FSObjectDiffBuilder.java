package org.vaultage.server.core.diff;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.FSObjectBuildException;
import org.vaultage.shared.fs.meta.IMeta;

public class FSObjectDiffBuilder {

  private static final Logger LOGGER = LoggerFactory.getLogger(FSObjectDiffBuilder.class);

  /**
   * Creates a {@link FSObject} containing {@link Diff} elements representing the differences
   * between the two given FSObjects containing {@link IMeta} elements. The set roots must be the
   * same.
   * 
   * @param fsObjOld
   *          old FSObject. May not be null.
   * @param fsObjNew
   *          new FSObject. May not be null.
   * @return built {@link FSObject}
   * @throws FSObjectBuildException
   */
  public FSObject<Diff> build(FSObject<IMeta> fsObjOld, FSObject<IMeta> fsObjNew)
      throws FSObjectBuildException {
    LOGGER.debug("Creating FSObjectDiff for '" + fsObjOld + "' and '" + fsObjNew + "'");
    if (fsObjOld.getRoot().equals(fsObjNew.getRoot())) {
      try {
        FSObject<Diff> fsObjDiff = createFSObjectDiff(fsObjOld, fsObjNew);
        LOGGER.trace("FSObjectDiff built '" + fsObjDiff + "'");
        return fsObjDiff;
      } catch (Exception exc) {
        throw new FSObjectBuildException("Building FSObjectDiff failed due to exception", exc);
      }
    } else {
      throw new FSObjectBuildException("Set roots not equal for '" + fsObjOld.getRoot() + "' and '"
          + fsObjNew.getRoot() + "'");
    }
  }

  private FSObject<Diff> createFSObjectDiff(FSObject<IMeta> fsObjOld, FSObject<IMeta> fsObjNew) {
    FSObject<Diff> fsObjDiff = new FSObject<>(fsObjNew.getRoot());
    Map<String, IMeta> mapNew = fsObjNew.getClonedMap();
    for (Entry<String, IMeta> entryOld : fsObjOld.getUnmodifiableMap().entrySet()) {
      Diff diff = DiffFactory.getDiff(entryOld.getValue(), mapNew.remove(entryOld.getKey()));
      fsObjDiff.put(entryOld.getKey(), diff);
    }
    for (Entry<String, IMeta> entryNew : mapNew.entrySet()) {
      Diff diff = DiffFactory.getDiff(null, entryNew.getValue());
      fsObjDiff.put(entryNew.getKey(), diff);
    }
    return fsObjDiff;
  }

}
