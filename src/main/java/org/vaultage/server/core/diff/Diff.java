package org.vaultage.server.core.diff;

import java.io.Serializable;
import java.util.Objects;

import org.vaultage.shared.fs.meta.IMeta;

public class Diff implements Serializable {

  private static final long serialVersionUID = 201302070149L;

  private final DiffType diffType;
  private final IMeta meta;
  private final IMeta metaOld;

  Diff(DiffType diffType, IMeta meta) {
    this(diffType, meta, null);
  }

  Diff(DiffType diffType, IMeta meta, IMeta metaOld) {
    this.diffType = Objects.requireNonNull(diffType);
    this.meta = Objects.requireNonNull(meta);
    this.metaOld = metaOld;
  }

  public DiffType getDiffType() {
    return diffType;
  }

  public IMeta getMeta() {
    return meta;
  }

  public IMeta getMetaOld() {
    return metaOld;
  }

  @Override
  public int hashCode() {
    return Objects.hash(diffType, meta, metaOld);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Diff) {
      Diff other = (Diff) obj;
      return Objects.equals(diffType, other.diffType) && Objects.equals(meta, other.meta)
          && Objects.equals(metaOld, other.metaOld);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "Diff [diffType=" + diffType + ", meta=" + meta + ", metaOld=" + metaOld + "]";
  }

}
