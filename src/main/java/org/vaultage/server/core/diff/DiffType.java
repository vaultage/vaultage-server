package org.vaultage.server.core.diff;

public enum DiffType {

  CREATED, DELETED, UNCHANGED, MODIFIED;

}
