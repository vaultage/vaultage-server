package org.vaultage.server.core.diff;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.shared.fs.meta.IMeta;

public class DiffFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(DiffFactory.class);

  public static final Diff getDiff(IMeta metaOld, IMeta metaNew) {
    Diff ret;
    if ((metaOld == null) && (metaNew == null)) {
      throw new NullPointerException("metaOld and metaNew are null");
    } else if ((metaOld == null) && (metaNew != null)) {
      ret = new Diff(DiffType.CREATED, metaNew);
    } else if ((metaOld != null) && (metaNew == null)) {
      ret = new Diff(DiffType.DELETED, metaOld);
    } else if (isUnchanged(metaOld, metaNew)) {
      ret = new Diff(DiffType.UNCHANGED, metaNew);
    } else {
      ret = new Diff(DiffType.MODIFIED, metaNew, metaOld);
    }
    LOGGER.trace("Diff determined '" + ret + "' for metaOld '" + metaOld + "' and metaNew '"
        + metaNew + "'");
    return ret;
  }

  private static boolean isUnchanged(IMeta metaOld, IMeta metaNew) {
    if (Objects.equals(metaOld.getPath(), metaNew.getPath())) {
      return Objects.equals(metaOld.getHash(), metaNew.getHash());
    } else {
      throw new IllegalArgumentException("Paths not equal: '" + metaOld + "' and '" + metaNew + "'");
    }
  }

}
