package org.vaultage.server.core;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.Authenticator;
import org.vaultage.server.comm.auth.ClientIdentity;
import org.vaultage.server.core.sync.SyncException;
import org.vaultage.server.core.sync.SyncManager;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.shared.comm.auth.Auth;
import org.vaultage.shared.comm.auth.AuthChallenge;
import org.vaultage.shared.comm.auth.AuthRequest;
import org.vaultage.shared.comm.auth.AuthResponse;
import org.vaultage.shared.comm.command.ServerCommand;
import org.vaultage.shared.comm.command.ServerCommandType;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.FileData;
import org.vaultage.shared.fs.meta.IMeta;

public class Delegator {

  private static final Logger LOGGER = LoggerFactory.getLogger(Delegator.class);

  private final Authenticator authenticator;
  private final SyncManager syncManager;

  public Delegator(Authenticator authenticator, SyncManager syncManager) {
    this.authenticator = Objects.requireNonNull(authenticator);
    this.syncManager = Objects.requireNonNull(syncManager);
  }

  @SuppressWarnings("unchecked")
  public Object delegate(ClientIdentity identity, ServerCommand<?> command) throws Exception {
    LOGGER.info("" + identity + ": received '" + command + "'");
    Object response;
    if (identity.isAuthenticated()) {
      response = delegate(command, identity);
    } else if (command.getType() == ServerCommandType.AuthRequestCommand) {
      response = delegateAuthRequestCommand((ServerCommand<AuthRequest>) command, identity);
    } else if (command.getType() == ServerCommandType.AuthCommand) {
      response = delegateAuthCommand((ServerCommand<Auth>) command, command.getSender());
    } else {
      throw new IllegalArgumentException("Non-AuthCommand received for locked client: '" + command
          + "'");
    }
    return response;
  }

  @SuppressWarnings("unchecked")
  private Object delegate(ServerCommand<?> command, ClientIdentity identity) throws SyncException {
    Object response = null;
    switch (command.getType()) {
    case SyncRequestCommand:
      delegateSyncRequestCommand((ServerCommand<FSObject<IMeta>>) command, identity);
      break;
    case FileSendCommand:
      delegateFileSendCommand((ServerCommand<FileData>) command, identity);
      break;
    case SyncStopCommand:
      delegateSyncStopCommand((ServerCommand<String>) command, identity);
      break;
    case TestCommand:
      break;
    default:
      LOGGER.warn("Received invalid command '" + command + "'");
    }
    return response;
  }

  private void delegateSyncRequestCommand(ServerCommand<FSObject<IMeta>> command,
      ClientIdentity identity) throws SyncException {
    syncManager.enroll(identity, command.getData());
    // TODO ret
  }

  private void delegateFileSendCommand(ServerCommand<FileData> command, ClientIdentity identity)
      throws SyncException {
    // TODO ret
    syncManager.getSyncSupervisor(identity).requested(command.getData());
  }

  private void delegateSyncStopCommand(ServerCommand<String> command, ClientIdentity identity) {
    // TODO
    LOGGER.error("SyncStopCommand not implemented");
  }

  private AuthChallenge delegateAuthRequestCommand(ServerCommand<AuthRequest> command,
      ClientIdentity identity) throws DatabaseException {
    return authenticator.challenge(identity, command.getData(), command.getSender());
  }

  private AuthResponse delegateAuthCommand(ServerCommand<Auth> command, String host) {
    return authenticator.authenticate(command.getData(), host);
  }

}
