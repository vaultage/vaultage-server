package org.vaultage.server;

import java.io.IOException;
import java.nio.file.Path;

import org.apache.commons.configuration.ConfigurationException;
import org.vaultage.shared.comm.auth.CodecUtils;

import ch.marcsladek.commons.util.config.Config;
import ch.marcsladek.commons.util.config.Property;

public class ServerConfig extends Config {

  private static final Path CONFIG_FILE = Server.DIR.resolve("config.properties");

  public static final Property PORT = new Property("connection.port", 12963);
  public static final Property CLIENT_TIMEOUT = new Property("connection.client-timeout", 0);
  public static final Property ADMIN_PASSWORD = new Property("setup.admin-password",
      CodecUtils.generateRandomString(32));
  public static final Property DATABASE_PATH = new Property("database.path",
      Server.DIR.resolve("database/vaultage"));
  public static final Property DATABASE_USER = new Property("database.user", "dbadmin");
  public static final Property DATABASE_PASSWORD = new Property("database.password",
      CodecUtils.generateRandomString(32));
  public static final Property DATABASE_SECRET = new Property("database.secret",
      CodecUtils.generateRandomString(64));
  public static final Property SYNC_MANAGER_ALLOWED_TIME = new Property("syncmanager.allowed-time",
      60);

  ServerConfig() throws ConfigurationException, IllegalAccessException, IOException {
    super(CONFIG_FILE, ServerConfig.class);
    this.createDirs();
  }

}
