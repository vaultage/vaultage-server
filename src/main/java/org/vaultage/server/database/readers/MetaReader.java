package org.vaultage.server.database.readers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.classes.Data;
import org.vaultage.server.database.classes.Meta;
import org.vaultage.server.database.classes.Meta.MetaField;

import ch.marcsladek.commons.hibernate.Field;
import ch.marcsladek.commons.hibernate.query.DatabaseQuery;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryBuilder;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryType;

public class MetaReader extends AbstractReader<Meta> {

  private static final Logger LOGGER = LoggerFactory.getLogger(MetaReader.class);

  private final DatabaseQuery QUERY_ISNOTFILE;
  private final DatabaseQuery QUERY_ISFILE;

  public MetaReader() {
    super(Meta.class);
    QUERY_ISNOTFILE = getQuery(false);
    QUERY_ISFILE = getQuery(true);
  }

  private static DatabaseQuery getQuery(boolean isFile) {
    DatabaseQueryBuilder queryBuilder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        Meta.class);
    queryBuilder.addField(MetaField.path);
    queryBuilder.addField(MetaField.changeDate);
    queryBuilder.addField(MetaField.isFile);
    if (isFile) {
      queryBuilder.addField(MetaField.data);
    }
    return queryBuilder.build();
  }

  public Meta getMetaIsNotFile(Session session, String path, Date changeDate)
      throws DatabaseReadException {
    Map<Field, Object> binds = new HashMap<>();
    binds.put(MetaField.path, Objects.requireNonNull(path));
    binds.put(MetaField.changeDate, Objects.requireNonNull(changeDate));
    binds.put(MetaField.isFile, false);
    return getUnique(session, QUERY_ISNOTFILE.getBindedDatabaseQuery(binds));
  }

  public Meta getMetaIsFile(Session session, String path, Date changeDate, Data data)
      throws DatabaseReadException {
    Map<Field, Object> binds = new HashMap<>();
    binds.put(MetaField.path, Objects.requireNonNull(path));
    binds.put(MetaField.changeDate, Objects.requireNonNull(changeDate));
    binds.put(MetaField.isFile, true);
    binds.put(MetaField.data, Objects.requireNonNull(data));
    return getUnique(session, QUERY_ISFILE.getBindedDatabaseQuery(binds));
  }

  @Override
  Logger getLogger() {
    return LOGGER;
  }

}
