package org.vaultage.server.database.readers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.classes.MetaContainer;
import org.vaultage.server.database.classes.MetaContainer.MetaContainerField;
import org.vaultage.server.database.classes.User;

import ch.marcsladek.commons.hibernate.Field;
import ch.marcsladek.commons.hibernate.query.DatabaseQuery;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryBuilder;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryType;

public class MetaContainerReader extends AbstractReader<MetaContainer> {

  private static final Logger LOGGER = LoggerFactory.getLogger(MetaContainerReader.class);

  private final DatabaseQuery QUERY_USER_LAST;

  public MetaContainerReader() {
    super(MetaContainer.class);
    QUERY_USER_LAST = getQueryUserLast();
  }

  private static DatabaseQuery getQueryUserLast() {
    DatabaseQueryBuilder queryBuilder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        MetaContainer.class);
    queryBuilder.addField(MetaContainerField.user);
    queryBuilder.addField(MetaContainerField.status);
    queryBuilder.setOrder(Arrays.asList(new Field[] { MetaContainerField.date }), false);
    queryBuilder.setLimit(1);
    return queryBuilder.build();
  }

  public MetaContainer getLastMetaContainer(Session session, Long userId)
      throws DatabaseReadException {
    Objects.requireNonNull(userId);
    User user = DatabaseAccess.getReaderProvider().getUserReader().getForId(session, userId);
    if (user != null) {
      Map<Field, Object> binds = new HashMap<>();
      binds.put(MetaContainerField.user, user);
      binds.put(MetaContainerField.status, MetaContainer.STATUS_OK);
      return getUnique(session, QUERY_USER_LAST.getBindedDatabaseQuery(binds));
    } else {
      throw new DatabaseReadException("User does not exist for id '" + userId + "'");
    }
  }

  @Override
  Logger getLogger() {
    return LOGGER;
  }

}
