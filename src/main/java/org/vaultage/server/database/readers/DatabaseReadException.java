package org.vaultage.server.database.readers;

import org.vaultage.server.database.DatabaseException;

public class DatabaseReadException extends DatabaseException {

  private static final long serialVersionUID = 201311162138L;

  public DatabaseReadException() {
    super();
  }

  public DatabaseReadException(String msg) {
    super(msg);
  }

  public DatabaseReadException(String msg, Throwable cause) {
    super(msg, cause);
  }

}