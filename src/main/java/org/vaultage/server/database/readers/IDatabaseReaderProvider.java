package org.vaultage.server.database.readers;


public interface IDatabaseReaderProvider {

  public <T> IDatabaseReader<T> getReader(Class<T> clazz);

  public UserReader getUserReader();

  public MetaContainerReader getMetaContainerReader();

  public MetaReader getMetaReader();

  public DataReader getDataReader();

}
