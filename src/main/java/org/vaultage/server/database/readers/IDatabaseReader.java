package org.vaultage.server.database.readers;

import org.hibernate.Session;

public interface IDatabaseReader<T> {

  public Class<T> getEntityClass();

  public Long count(Session session) throws DatabaseReadException;

  public T getForId(Session session, Long id) throws DatabaseReadException;

}
