package org.vaultage.server.database.readers;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.classes.User;
import org.vaultage.server.database.classes.User.UserField;

import ch.marcsladek.commons.hibernate.Field;
import ch.marcsladek.commons.hibernate.query.DatabaseQuery;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryBuilder;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryType;

public class UserReader extends AbstractReader<User> {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserReader.class);

  private final DatabaseQuery QUERY_NAME;
  private final DatabaseQuery QUERY_NAME_HASH;

  public UserReader() {
    super(User.class);
    QUERY_NAME = getQueryName();
    QUERY_NAME_HASH = getQueryNameHash();
  }

  private static DatabaseQuery getQueryName() {
    DatabaseQueryBuilder queryBuilder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        User.class);
    queryBuilder.addField(UserField.name);
    return queryBuilder.build();
  }

  private static DatabaseQuery getQueryNameHash() {
    DatabaseQueryBuilder queryBuilder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        User.class);
    queryBuilder.addField(UserField.name);
    queryBuilder.addField(UserField.hash);
    return queryBuilder.build();
  }

  /**
   * gets user defined by its name
   * 
   * @param name
   *          may not be null
   * @return user or null if not existent
   * @throws DatabaseException
   *           if there was an error or inconsistency in the database
   */
  public User getUser(Session session, String name) throws DatabaseReadException {
    Map<Field, Object> binds = new HashMap<>();
    binds.put(UserField.name, Objects.requireNonNull(name));
    return getUnique(session, QUERY_NAME.getBindedDatabaseQuery(binds));
  }

  /**
   * gets user defined by its name and hash
   * 
   * @param name
   *          may not be null
   * @param hash
   *          may not be null
   * @return user or null if not existent
   * @throws DatabaseException
   *           if there was an error or inconsistency in the database
   */
  User getUser(Session session, String name, String hash) throws DatabaseReadException {
    Map<Field, Object> binds = new HashMap<>();
    binds.put(UserField.name, Objects.requireNonNull(name));
    binds.put(UserField.hash, Objects.requireNonNull(hash));
    return getUnique(session, QUERY_NAME_HASH.getBindedDatabaseQuery(binds));
  }

  @Override
  Logger getLogger() {
    return LOGGER;
  }

}
