package org.vaultage.server.database.readers;

import java.util.HashMap;
import java.util.Map;

import org.vaultage.server.database.classes.Data;
import org.vaultage.server.database.classes.Meta;
import org.vaultage.server.database.classes.MetaContainer;
import org.vaultage.server.database.classes.User;

public class DatabaseReaderProvider implements IDatabaseReaderProvider {

  private final Map<Class<?>, IDatabaseReader<?>> readerMap;

  public DatabaseReaderProvider() {
    readerMap = new HashMap<Class<?>, IDatabaseReader<?>>();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T> IDatabaseReader<T> getReader(Class<T> clazz) {
    IDatabaseReader<T> reader = (IDatabaseReader<T>) readerMap.get(clazz);
    if (reader == null) {
      readerMap.put(clazz, reader = getNewReader(clazz));
    }
    return reader;
  }

  @SuppressWarnings("unchecked")
  private <T> IDatabaseReader<T> getNewReader(Class<T> clazz) {
    IDatabaseReader<?> reader;
    if (clazz == User.class) {
      reader = new UserReader();
    } else if (clazz == MetaContainer.class) {
      reader = new MetaContainerReader();
    } else if (clazz == Meta.class) {
      reader = new MetaReader();
    } else if (clazz == Data.class) {
      reader = new DataReader();
    } else {
      throw new IllegalArgumentException("No reader available for class '" + clazz + "'");
    }
    return (IDatabaseReader<T>) reader;
  }

  public UserReader getUserReader() {
    return (UserReader) getReader(User.class);
  }

  public MetaContainerReader getMetaContainerReader() {
    return (MetaContainerReader) getReader(MetaContainer.class);
  }

  public MetaReader getMetaReader() {
    return (MetaReader) getReader(Meta.class);
  }

  public DataReader getDataReader() {
    return (DataReader) getReader(Data.class);
  }

}
