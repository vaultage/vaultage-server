package org.vaultage.server.database.readers;

import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.vaultage.server.database.DatabaseUtils;

import ch.marcsladek.commons.hibernate.Field;
import ch.marcsladek.commons.hibernate.query.BindedDatabaseQuery;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryBuilder;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryType;

public abstract class AbstractReader<T> implements IDatabaseReader<T> {

  private final Class<T> entityClass;

  private BindedDatabaseQuery countQuery;

  AbstractReader(Class<T> entityClass) {
    this.entityClass = Objects.requireNonNull(entityClass);
  }

  public Class<T> getEntityClass() {
    return entityClass;
  }

  public Long count(Session session) throws DatabaseReadException {
    Objects.requireNonNull(session);
    String hql = getCountQuery().getQueryString();
    try {
      Query query = session.createQuery(hql);
      Long ret = DatabaseUtils.uniqueResult(query);
      getLogger().debug("Returned '" + ret + "' for count '" + entityClass.getName() + "'");
      return ret;
    } catch (HibernateException exc) {
      throw new DatabaseReadException("Error for count '" + entityClass.getName() + "'", exc);
    }
  }

  private BindedDatabaseQuery getCountQuery() {
    if (countQuery == null) {
      countQuery = new DatabaseQueryBuilder(DatabaseQueryType.COUNT, entityClass).build()
          .getBindedDatabaseQuery();
    }
    return countQuery;
  }

  /**
   * gets entity for given id
   * 
   * @param id
   * @return
   * @throws DatabaseReadException
   *           if there was an error reading from the database
   */
  @SuppressWarnings("unchecked")
  public T getForId(Session session, Long id) throws DatabaseReadException {
    Objects.requireNonNull(session);
    try {
      T ret = (T) session.get(entityClass, id);
      getLogger().debug("Returned '" + ret + "' for get user '" + id + "'");
      return ret;
    } catch (HibernateException exc) {
      throw new DatabaseReadException("Error get user '" + id + "'", exc);
    }
  }

  List<T> get(Session session, BindedDatabaseQuery dbQuery) throws DatabaseReadException {
    Objects.requireNonNull(session);
    try {
      Query query = session.createQuery(dbQuery.getQueryString());
      for (Entry<Field, Object> bind : dbQuery.getBinds()) {
        bindValue(query, bind.getKey().toString(), bind.getValue());
      }
      if (dbQuery.getLimit() > 0) {
        query.setMaxResults(dbQuery.getLimit());
      }
      List<T> ret = DatabaseUtils.list(query);
      getLogger().debug("Returned '" + ret + "' for query '" + dbQuery + "'");
      return ret;
    } catch (HibernateException exc) {
      throw new DatabaseReadException("Error for query '" + dbQuery + "'", exc);
    }
  }

  private void bindValue(Query query, String name, Object obj) {
    if (obj instanceof String) {
      query.setString(name, (String) obj);
    } else if (obj instanceof Integer) {
      query.setInteger(name, (Integer) obj);
    } else if (obj instanceof Long) {
      query.setLong(name, (Long) obj);
    } else if (obj instanceof Boolean) {
      query.setBoolean(name, (Boolean) obj);
    } else if (obj instanceof Date) {
      query.setDate(name, (Date) obj);
    } else {
      query.setEntity(name, obj);
    }
  }

  T getUnique(Session session, BindedDatabaseQuery dbQuery) throws DatabaseReadException {
    List<T> list = get(session, dbQuery);
    if (list.size() == 1) {
      return list.get(0);
    } else if (list.size() == 0) {
      return null;
    } else {
      throw new DatabaseReadException("Equal entries in database: '" + list + "'");
    }
  }

  abstract Logger getLogger();

}
