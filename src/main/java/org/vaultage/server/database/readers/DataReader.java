package org.vaultage.server.database.readers;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.classes.Data;
import org.vaultage.server.database.classes.Data.DataField;

import ch.marcsladek.commons.hibernate.Field;
import ch.marcsladek.commons.hibernate.query.DatabaseQuery;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryBuilder;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryType;

public class DataReader extends AbstractReader<Data> {

  private static final Logger LOGGER = LoggerFactory.getLogger(DataReader.class);

  private final DatabaseQuery QUERY_HASH;

  public DataReader() {
    super(Data.class);
    QUERY_HASH = getQueryHash();
  }

  private static DatabaseQuery getQueryHash() {
    DatabaseQueryBuilder queryBuilder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        Data.class);
    queryBuilder.addField(DataField.hash);
    return queryBuilder.build();
  }

  public Data getDataForHash(Session session, String hash) throws DatabaseReadException {
    Map<Field, Object> binds = new HashMap<>();
    binds.put(DataField.hash, Objects.requireNonNull(hash));
    return getUnique(session, QUERY_HASH.getBindedDatabaseQuery(binds));
  }

  @Override
  Logger getLogger() {
    return LOGGER;
  }
}
