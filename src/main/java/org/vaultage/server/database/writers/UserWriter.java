package org.vaultage.server.database.writers;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.classes.User;
import org.vaultage.server.database.readers.UserReader;

public class UserWriter extends AbstractWriter<User> {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserWriter.class);

  public UserWriter() {
    super(User.class);
  }

  public Long writeNewUser(Session session, String name, String hash, String salt)
      throws DatabaseException {
    requireNonNull(name, hash, salt);
    User user = ((UserReader) DatabaseAccess.getReaderProvider().getReader(User.class)).getUser(
        session, name);
    if (user == null) {
      return save(session, new User(name, hash, salt));
    } else {
      throw new DatabaseWriteException("Error saving user, name '" + name + "' already exists");
    }
  }

  @Override
  Logger getLogger() {
    return LOGGER;
  }

}
