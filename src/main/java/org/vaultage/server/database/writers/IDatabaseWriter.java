package org.vaultage.server.database.writers;

import org.hibernate.Session;

public interface IDatabaseWriter<T> {

  public Class<T> getEntityClass();

  public Long save(Session session, T entity) throws DatabaseWriteException;

  public void update(Session session, T entity) throws DatabaseWriteException;

}
