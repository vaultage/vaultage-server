package org.vaultage.server.database.writers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.classes.Meta;

public class MetaWriter extends AbstractWriter<Meta> {

  private static final Logger LOGGER = LoggerFactory.getLogger(MetaWriter.class);

  public MetaWriter() {
    super(Meta.class);
  }

  @Override
  Logger getLogger() {
    return LOGGER;
  }

}
