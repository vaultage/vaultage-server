package org.vaultage.server.database.writers;

import org.vaultage.server.database.DatabaseException;

public class DatabaseWriteException extends DatabaseException {

  private static final long serialVersionUID = 201311161946L;

  public DatabaseWriteException() {
    super();
  }

  public DatabaseWriteException(String msg) {
    super(msg);
  }

  public DatabaseWriteException(String msg, Throwable cause) {
    super(msg, cause);
  }

}