package org.vaultage.server.database.writers;

import java.util.Objects;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;

public abstract class AbstractWriter<T> implements IDatabaseWriter<T> {

  private final Class<T> entityClass;

  AbstractWriter(Class<T> entityClass) {
    this.entityClass = Objects.requireNonNull(entityClass);
  }

  public Class<T> getEntityClass() {
    return entityClass;
  }

  public Long save(Session session, T entity) throws DatabaseWriteException {
    requireNonNull(session, entity);
    try {
      Long ret = (Long) session.save(entity);
      getLogger().debug("Returned '" + ret + "' for saving '" + entity + "'");
      return ret;
    } catch (HibernateException exc) {
      throw new DatabaseWriteException("Error saving '" + entity + "'", exc);
    }
  }

  public void update(Session session, T entity) throws DatabaseWriteException {
    requireNonNull(session, entity);
    try {
      session.update(entity);
      getLogger().debug("Updating successful for '" + entity + "'");
    } catch (HibernateException exc) {
      throw new DatabaseWriteException("Error updating '" + entity + "'", exc);
    }
  }

  abstract Logger getLogger();

  static void requireNonNull(Object... objs) {
    for (Object obj : objs) {
      Objects.requireNonNull(obj);
    }
  }

}
