package org.vaultage.server.database.writers;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.classes.MetaContainer;
import org.vaultage.server.database.classes.User;

public class MetaContainerWriter extends AbstractWriter<MetaContainer> {

  private static final Logger LOGGER = LoggerFactory.getLogger(MetaContainerWriter.class);

  public MetaContainerWriter() {
    super(MetaContainer.class);
  }

  public Long writeMetaContainerForUser(Session session, Long userId, MetaContainer container)
      throws DatabaseException {
    requireNonNull(userId, container);
    User user = DatabaseAccess.getReaderProvider().getUserReader().getForId(session, userId);
    if (user != null) {
      container.setUser(user);
      return save(session, container);
    } else {
      throw new DatabaseWriteException("writeFSObject: user does not exist for id '" + userId + "'");
    }
  }

  @Override
  Logger getLogger() {
    return LOGGER;
  }

}
