package org.vaultage.server.database.writers;

import java.util.HashMap;
import java.util.Map;

import org.vaultage.server.database.classes.Data;
import org.vaultage.server.database.classes.Meta;
import org.vaultage.server.database.classes.MetaContainer;
import org.vaultage.server.database.classes.User;

public class DatabaseWriterProvider implements IDatabaseWriterProvider {

  private final Map<Class<?>, IDatabaseWriter<?>> writerMap;

  public DatabaseWriterProvider() {
    writerMap = new HashMap<Class<?>, IDatabaseWriter<?>>();
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T> IDatabaseWriter<T> getWriter(Class<T> clazz) {
    IDatabaseWriter<T> writer = (IDatabaseWriter<T>) writerMap.get(clazz);
    if (writer == null) {
      writerMap.put(clazz, writer = getNewWriter(clazz));
    }
    return writer;
  }

  @SuppressWarnings("unchecked")
  private <T> IDatabaseWriter<T> getNewWriter(Class<T> clazz) {
    IDatabaseWriter<?> writer;
    if (clazz == User.class) {
      writer = new UserWriter();
    } else if (clazz == MetaContainer.class) {
      writer = new MetaContainerWriter();
    } else if (clazz == Meta.class) {
      writer = new MetaWriter();
    } else if (clazz == Data.class) {
      writer = new DataWriter();
    } else {
      throw new IllegalArgumentException("No writer available for class '" + clazz + "'");
    }
    return (IDatabaseWriter<T>) writer;
  }

  public UserWriter getUserWriter() {
    return (UserWriter) getWriter(User.class);
  }

  public MetaContainerWriter getMetaContainerWriter() {
    return (MetaContainerWriter) getWriter(MetaContainer.class);
  }

  public MetaWriter getMetaWriter() {
    return (MetaWriter) getWriter(Meta.class);
  }

  public DataWriter getDataWriter() {
    return (DataWriter) getWriter(Data.class);
  }

}
