package org.vaultage.server.database.writers;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.DatabaseUtils;
import org.vaultage.server.database.classes.Data;
import org.vaultage.server.database.classes.Meta;

public class DataWriter extends AbstractWriter<Data> {

  private static final Logger LOGGER = LoggerFactory.getLogger(DataWriter.class);

  public DataWriter() {
    super(Data.class);
  }

  public Long writeData(Session session, Meta meta, String hash, byte[] dataArray)
      throws DatabaseException {
    requireNonNull(meta, hash, dataArray);
    if (meta.getIsFile()) {
      Long ret = null;
      Data data = DatabaseAccess.getReaderProvider().getDataReader().getDataForHash(session, hash);
      if (data == null) {
        data = new Data(hash, DatabaseUtils.createBlob(dataArray));
        ret = save(session, data);
      } else {
        LOGGER.info("writeData: data already exists '" + data + "'");
      }
      meta.setData(data);
      DatabaseAccess.getWriterProvider().getMetaWriter().update(session, meta);
      return ret != null ? ret : data.getId();
    } else {
      throw new DatabaseWriteException("writeData: invalid meta '" + meta + "' for id '" + meta
          + "'");
    }
  }

  @Override
  Logger getLogger() {
    return LOGGER;
  }

}
