package org.vaultage.server.database.writers;


public interface IDatabaseWriterProvider {

  public <T> IDatabaseWriter<T> getWriter(Class<T> clazz);

  public UserWriter getUserWriter();

  public MetaContainerWriter getMetaContainerWriter();

  public MetaWriter getMetaWriter();

  public DataWriter getDataWriter();

}
