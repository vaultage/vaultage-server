package org.vaultage.server.database;

import java.util.Date;
import java.util.HashSet;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.classes.Data;
import org.vaultage.server.database.classes.Meta;
import org.vaultage.server.database.classes.MetaContainer;
import org.vaultage.server.database.classes.User;
import org.vaultage.server.database.readers.DataReader;
import org.vaultage.server.database.readers.DatabaseReadException;
import org.vaultage.server.database.readers.MetaReader;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.meta.IMeta;
import org.vaultage.shared.fs.meta.MetaFile;
import org.vaultage.shared.fs.meta.MetaFolder;

public class DatabaseObjectConverter {

  private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseObjectConverter.class);

  public static MetaContainer convert(FSObject<IMeta> fsObj, User user, Date date) {
    try {
      return convertWithDBCheck(null, fsObj, user, date);
    } catch (DatabaseReadException exc) {
      // silent catch
      return null;
    }
  }

  public static MetaContainer convertWithDBCheck(Session session, FSObject<IMeta> fsObj, User user,
      Date date) throws DatabaseReadException {
    MetaContainer container = new MetaContainer(user, fsObj.getRoot(), date, new HashSet<Meta>());
    for (IMeta meta : fsObj.getUnmodifiableMap().values()) {
      container.getElements().add(convert(session, meta));
    }
    return container;
  }

  private static Meta convert(Session session, IMeta meta) throws DatabaseReadException {
    Meta ret = null;
    Data data = null;
    if (session != null) {
      if (meta.isFolder()) {
        ret = getMetaReader().getMetaIsNotFile(session, meta.getPath(), meta.getChangeDate());
      } else {
        data = getDataReader().getDataForHash(session, meta.getHash());
        if (data != null) {
          ret = getMetaReader().getMetaIsFile(session, meta.getPath(), meta.getChangeDate(), data);
        }
      }
      LOGGER.trace("convert: got Meta from db '" + ret + "' for '" + meta + "'");
    }
    if (ret == null) {
      ret = new Meta(meta.getPath(), meta.getChangeDate(), !meta.isFolder());
      ret.setData(data);
      LOGGER.trace("convert: created new meta '" + ret + "'");
    }
    return ret;
  }

  private static MetaReader getMetaReader() {
    return DatabaseAccess.getReaderProvider().getMetaReader();
  }

  private static DataReader getDataReader() {
    return DatabaseAccess.getReaderProvider().getDataReader();
  }

  public static FSObject<IMeta> convert(MetaContainer metaContainer) {
    FSObject<IMeta> fsObj = new FSObject<>(metaContainer.getRoot());
    for (Meta meta : metaContainer.getElements()) {
      fsObj.put(meta.getPath(), convert(meta));
    }
    return fsObj;
  }

  public static IMeta convert(Meta meta) {
    if (meta.getIsFile()) {
      return new MetaFile(meta.getPath(), meta.getChangeDate(), meta.getData().getHash());
    } else {
      return new MetaFolder(meta.getPath(), meta.getChangeDate());
    }
  }

}
