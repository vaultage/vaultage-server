package org.vaultage.server.database;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.ClientIdentity;

public class SessionManager {

  private static final Logger LOGGER = LoggerFactory.getLogger(SessionManager.class);

  private final SessionFactory sessionFactory;
  private final Map<ClientIdentity, Session> sessionMap;

  public SessionManager(String database, String user, String password) throws DatabaseException {
    try {
      LOGGER.info("SessionFactory init for Database '" + database + "'");
      Configuration config = new Configuration();
      config.configure();
      config.setProperty("hibernate.connection.url", "jdbc:hsqldb:file:" + database);
      config.setProperty("hibernate.connection.username", user);
      config.setProperty("hibernate.connection.password", password);
      ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(
          config.getProperties()).buildServiceRegistry();
      sessionFactory = config.buildSessionFactory(serviceRegistry);
      sessionMap = new ConcurrentHashMap<>();
    } catch (HibernateException exc) {
      throw new DatabaseException("Unable to build SessionManager", exc);
    }
  }

  public SessionManager(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
    sessionMap = new ConcurrentHashMap<>();
  }

  Session newSession() throws DatabaseAccessException {
    if (!sessionFactory.isClosed()) {
      Session session = sessionFactory.openSession();
      session.beginTransaction();
      return session;
    } else {
      throw new DatabaseAccessException("SessionFactory has been closed");
    }
  }

  public Session newSession(ClientIdentity identity) throws DatabaseAccessException {
    if (!sessionFactory.isClosed()) {
      Session session = sessionMap.get(identity);
      if (session == null) {
        try {
          session = sessionFactory.openSession();
          session.beginTransaction();
          sessionMap.put(identity, session);
          return session;
        } catch (HibernateException exc) {
          throw new DatabaseAccessException("Unable to open session", exc);
        }
      } else {
        throw new DatabaseAccessException("Session already available for '" + identity + "'");
      }
    } else {
      throw new DatabaseAccessException("SessionFactory has been closed");
    }
  }

  public Session getSession(ClientIdentity identity) throws DatabaseAccessException {
    if (!sessionFactory.isClosed()) {
      Session session = sessionMap.get(identity);
      if (session != null) {
        return session;
      } else {
        throw new DatabaseAccessException("No session available for identity '" + identity + "'");
      }
    } else {
      throw new DatabaseAccessException("SessionFactory has been closed");
    }
  }

  public void closeSession(ClientIdentity identity) {
    Session session = sessionMap.remove(identity);
    if (session != null) {
      try {
        session.close();
      } catch (HibernateException exc) {
        LOGGER.trace("Unable to close session '" + session + "'", exc);
      }
    }
  }

  void close() throws HibernateException {
    LOGGER.info("SessionManager closed");
    sessionMap.clear();
    sessionFactory.close();
  }

}