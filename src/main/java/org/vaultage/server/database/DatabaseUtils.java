package org.vaultage.server.database;

import java.io.Serializable;
import java.security.spec.InvalidKeySpecException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hsqldb.jdbc.JDBCBlob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.shared.comm.auth.CodecUtils;

public class DatabaseUtils {

  private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseUtils.class);

  public static <T> List<T> list(Query query) throws HibernateException {
    @SuppressWarnings("unchecked")
    List<T> list = query.list();
    return list;
  }

  public static <T> T uniqueResult(Query query) throws HibernateException {
    @SuppressWarnings("unchecked")
    T ret = (T) query.uniqueResult();
    return ret;
  }

  public static Blob createBlob(byte[] data) {
    try {
      return new JDBCBlob(Objects.requireNonNull(data));
    } catch (SQLException exc) {
      LOGGER.error("Cannot be thrown", exc);
      return null;
    }
  }

  public static Blob createBlob(Serializable serializable) {
    try {
      return new JDBCBlob(SerializationUtils.serialize(serializable));
    } catch (Exception exc) {
      LOGGER.error("Exception while serializing '" + serializable + "'", exc);
      return null;
    }
  }

  @SuppressWarnings("unchecked")
  public static <T> T resolveBlob(Blob blob) {
    try {
      return (T) SerializationUtils.deserialize(blob.getBinaryStream());
    } catch (Exception exc) {
      LOGGER.error("Exception while deserializing", exc);
      return null;
    }
  }

  public static Long createUser(Session session, String name, String password)
      throws InvalidKeySpecException, DatabaseException {
    String salt = CodecUtils.generateRandomString(128);
    String hash = CodecUtils.generateSaltedHash(password, salt);
    return DatabaseAccess.getWriterProvider().getUserWriter()
        .writeNewUser(session, name, hash, salt);
  }

}
