package org.vaultage.server.database;

public class DatabaseException extends Exception {

  private static final long serialVersionUID = 201311170304L;

  public DatabaseException() {
    super();
  }

  public DatabaseException(String msg) {
    super(msg);
  }

  public DatabaseException(String msg, Throwable cause) {
    super(msg, cause);
  }

}
