package org.vaultage.server.database;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.ClientIdentity;
import org.vaultage.server.database.readers.IDatabaseReaderProvider;
import org.vaultage.server.database.writers.IDatabaseWriterProvider;

import ch.marcsladek.commons.concurrent.Startable;

public final class DatabaseAccess implements Startable {

  private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseAccess.class);

  private static final String ADMIN_USERNAME = "admin";

  private static SessionManager sessionManager;
  private static IDatabaseReaderProvider readerProvider;
  private static IDatabaseWriterProvider writerProvider;

  private DatabaseAccess() {
  }

  public static void initialise(SessionManager manager, IDatabaseReaderProvider dbReaderProvider,
      IDatabaseWriterProvider dbWriterProvider, String adminPassword) {
    sessionManager = manager;
    readerProvider = dbReaderProvider;
    writerProvider = dbWriterProvider;
    if ((adminPassword != null) && (adminPassword.length() > 0)) {
      checkForAdmin(adminPassword);
    }
  }

  public static Session getSession(ClientIdentity identity) throws DatabaseAccessException {
    return sessionManager.getSession(identity);
  }

  public static IDatabaseReaderProvider getReaderProvider() {
    return readerProvider;
  }

  public static IDatabaseWriterProvider getWriterProvider() {
    return writerProvider;
  }

  public static Startable getDatabaseAccessStartable() {
    return new DatabaseAccess();
  }

  @Override
  public void start() throws Exception {
    if (sessionManager == null) {
      throw new IllegalStateException("Factory not initalised");
    }
  }

  @Override
  public boolean isRunning() {
    return sessionManager != null;
  }

  @Override
  public void shutdown() {
    LOGGER.info("SessionFactory closed");
    sessionManager.close();
    readerProvider = null;
    writerProvider = null;
  }

  @Override
  public boolean isTerminated() {
    return sessionManager == null;
  }

  private static void checkForAdmin(String adminPassword) {
    try {
      Session session = sessionManager.newSession();
      try {
        DatabaseUtils.createUser(session, ADMIN_USERNAME, adminPassword);
        session.getTransaction().commit();
        LOGGER.info("admin user has been created");
      } catch (Exception exc) {
        session.getTransaction().rollback();
        LOGGER.debug("admin user already exists");
      }
      session.close();
    } catch (Exception exc) {
      LOGGER.error("Unable to access database", exc);
    }
  }
}
