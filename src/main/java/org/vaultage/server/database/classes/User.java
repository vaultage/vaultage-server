package org.vaultage.server.database.classes;

import java.util.Objects;

import ch.marcsladek.commons.hibernate.Field;

public class User {

  public enum UserField implements Field {
    name, hash, salt;
  }

  private Long id;
  private String name;
  private String hash;
  private String salt;

  public User() {
  }

  public User(String name, String hash, String salt) {
    this.name = name;
    this.hash = hash;
    this.salt = salt;
  }

  public Long getId() {
    return id;
  }

  @SuppressWarnings("unused")
  private void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  public String getSalt() {
    return salt;
  }

  public void setSalt(String salt) {
    this.salt = salt;
  }

  @Override
  public String toString() {
    return "User [id=" + id + ", name=" + name + ", hash=" + hash + ", salt=" + salt + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, hash, salt);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj instanceof User) {
      User other = (User) obj;
      return Objects.equals(name, other.name) && Objects.equals(hash, other.hash)
          && Objects.equals(salt, other.salt);
    } else {
      return false;
    }
  }

}
