package org.vaultage.server.database.classes;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

import ch.marcsladek.commons.hibernate.Field;

public class MetaContainer {

  public final static int STATUS_INIT = 0;
  public final static int STATUS_OK = 1;
  public final static int STATUS_ERROR = 2;

  public enum MetaContainerField implements Field {
    user, root, date, status, elements;
  }

  private Long id;
  private User user;
  private String root;
  private Date date;
  private Integer status;
  private Set<Meta> elements;

  public MetaContainer() {
  }

  public MetaContainer(User user, String root, Date date, Set<Meta> elements) {
    this.user = user;
    this.root = root;
    this.date = date;
    this.status = STATUS_INIT;
    this.elements = elements;
  }

  public Long getId() {
    return id;
  }

  @SuppressWarnings("unused")
  private void setId(Long id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getRoot() {
    return root;
  }

  public void setRoot(String root) {
    this.root = root;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Set<Meta> getElements() {
    return elements;
  }

  public void setElements(Set<Meta> elements) {
    this.elements = elements;
  }

  @Override
  public String toString() {
    return "MetaContainer [id=" + id + ", user=" + user + ", root=" + root + ", date=" + date
        + ", status=" + status + ", nElements=" + elements.size() + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, root, date);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj instanceof MetaContainer) {
      MetaContainer other = (MetaContainer) obj;
      return Objects.equals(user, other.user) && Objects.equals(root, other.root)
          && Objects.equals(date, other.date);
    } else {
      return false;
    }
  }

}
