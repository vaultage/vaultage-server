package org.vaultage.server.database.classes;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.Objects;

import ch.marcsladek.commons.hibernate.Field;

public class Data {

  public enum DataField implements Field {
    hash, data;
  }

  private Long id;
  private String hash;
  private Blob data;

  public Data() {
  }

  public Data(String hash, Blob data) {
    this.hash = hash;
    this.data = data;
  }

  public Long getId() {
    return id;
  }

  @SuppressWarnings("unused")
  private void setId(Long id) {
    this.id = id;
  }

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  public Blob getData() {
    return data;
  }

  public byte[] getDataAsByteArray() {
    try {
      if (data != null) {
        return data.getBytes(1, (int) data.length());
      }
    } catch (SQLException exc) {
      // silent catch
    }
    return null;
  }

  public void setData(Blob data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "Data [id=" + id + ", hash=" + hash + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hash(hash);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj instanceof Data) {
      Data other = (Data) obj;
      return Objects.equals(hash, other.hash);
    } else {
      return false;
    }
  }

}
