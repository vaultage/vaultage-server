package org.vaultage.server.database.classes;

import java.util.Date;
import java.util.Objects;

import ch.marcsladek.commons.hibernate.Field;

public class Meta {

  public enum MetaField implements Field {
    path, changeDate, isFile, data;
  }

  private Long id;
  private String path;
  private Date changeDate;
  private Boolean isFile;
  private Data data;

  public Meta() {
  }

  public Meta(String path, Date changeDate, Boolean isFile) {
    this.path = path;
    this.changeDate = changeDate;
    this.isFile = isFile;
  }

  public Long getId() {
    return id;
  }

  @SuppressWarnings("unused")
  private void setId(Long id) {
    this.id = id;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Date getChangeDate() {
    return changeDate;
  }

  public void setChangeDate(Date changeDate) {
    this.changeDate = changeDate;
  }

  public Boolean getIsFile() {
    return isFile;
  }

  public void setIsFile(Boolean isFile) {
    this.isFile = isFile;
  }

  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "Meta [id=" + id + ", path=" + path + ", changeDate=" + changeDate + ", isFile="
        + isFile + ", data=" + data + "]";
  }

  @Override
  public int hashCode() {
    return Objects.hash(path, changeDate, isFile, data);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj instanceof Meta) {
      Meta other = (Meta) obj;
      return Objects.equals(path, other.path) && Objects.equals(changeDate, other.changeDate)
          && Objects.equals(isFile, other.isFile) && Objects.equals(data, other.data);
    } else {
      return false;
    }
  }

  // TODO toString, hashCode, equals

}
