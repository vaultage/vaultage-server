package org.vaultage.server.database;

public class DatabaseAccessException extends DatabaseException {

  private static final long serialVersionUID = 201402112140L;

  public DatabaseAccessException() {
    super();
  }

  public DatabaseAccessException(String msg) {
    super(msg);
  }

  public DatabaseAccessException(String msg, Throwable cause) {
    super(msg, cause);
  }

}
