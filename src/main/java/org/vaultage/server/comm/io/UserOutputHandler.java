package org.vaultage.server.comm.io;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.UserManager;
import org.vaultage.shared.comm.command.ClientCommand;
import org.vaultage.shared.comm.command.ClientCommandType;
import org.vaultage.shared.comm.command.ServerCommand;
import org.vaultage.shared.comm.command.ServerCommandType;

import ch.marcsladek.jrtnp.clientManager.ClientNotConnectedException;

public final class UserOutputHandler implements IOutputHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserOutputHandler.class);

  private final UserManager userManager;

  private static final Set<ClientCommandType> COMMAND_SET;
  private static final Map<ServerCommandType, ClientCommandType> RESPONSE_COMMAND_MAPPING;

  static {
    Map<ServerCommandType, ClientCommandType> map = new HashMap<>();
    map.put(ServerCommandType.AuthRequestCommand, ClientCommandType.AuthChallengeCommand);
    map.put(ServerCommandType.AuthCommand, ClientCommandType.AuthResponseCommand);
    map.put(ServerCommandType.SyncRequestCommand, ClientCommandType.SyncStartCommand);
    map.put(ServerCommandType.FileSendCommand, ClientCommandType.FileResponseCommand);
    map.put(ServerCommandType.SyncStopCommand, ClientCommandType.SyncEndCommand);
    RESPONSE_COMMAND_MAPPING = Collections.unmodifiableMap(map);

    Set<ClientCommandType> set = new HashSet<>();
    set.add(ClientCommandType.ErrorCommand);
    set.add(ClientCommandType.FileRequestCommand);
    set.add(ClientCommandType.SyncEndCommand);
    set.add(ClientCommandType.TestCommand);
    COMMAND_SET = Collections.unmodifiableSet(set);
  }

  public UserOutputHandler(UserManager userManager) {
    this.userManager = Objects.requireNonNull(userManager);
  }

  @Override
  public void handleResponseForCommand(ServerCommand<?> command, Object response) {
    try {
      if (response != null) {
        ClientCommand<?> clientCommand = getClientCommand(command, response);
        if (clientCommand != null) {
          send(clientCommand);
        } else {
          LOGGER.warn("No response for '" + command + "' with response '" + response + "'");
        }
      } else {
        LOGGER.error("Response is null for command '" + command + "'");
      }
    } catch (IOException | ClientNotConnectedException exc) {
      LOGGER.error("Unable sending response '" + command + "'for command '" + command + "'", exc);
    }
  }

  private ClientCommand<?> getClientCommand(ServerCommand<?> command, Object response)
      throws ClientNotConnectedException {
    ClientCommandType type = RESPONSE_COMMAND_MAPPING.get(command.getType());
    if (type != null) {
      String sender = userManager.getLocalAddressOf(command.getSender());
      return new ClientCommand<Object>(type, sender, command.getSender(), response);
    } else {
      return null;
    }
  }

  @Override
  public void handle(String receiver, ClientCommandType type, Object obj) {
    Objects.requireNonNull(receiver);
    if (COMMAND_SET.contains(type)) {
      try {
        String sender = userManager.getLocalAddressOf(receiver);
        send(new ClientCommand<Object>(type, sender, receiver, obj));
      } catch (IOException | ClientNotConnectedException exc) {
        LOGGER.error(type + ", " + obj, exc);
      }
    } else {
      throw new IllegalArgumentException("Illegal command type '" + type + "'");
    }
  }

  private void send(ClientCommand<?> command) throws IOException, ClientNotConnectedException {
    userManager.send(command.getReceiver(), command);
    LOGGER.info("Sent '" + command + "'");
  }

}
