package org.vaultage.server.comm.io;

import org.vaultage.shared.comm.command.ClientCommandType;
import org.vaultage.shared.comm.command.ServerCommand;

public interface IOutputHandler {

  public void handleResponseForCommand(ServerCommand<?> command, Object response);

  public void handle(String receiver, ClientCommandType type, Object obj);

}
