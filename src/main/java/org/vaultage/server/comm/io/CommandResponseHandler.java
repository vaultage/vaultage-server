package org.vaultage.server.comm.io;

import java.util.Objects;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.ClientIdentity;
import org.vaultage.server.comm.auth.UserManager;
import org.vaultage.server.core.Delegator;
import org.vaultage.server.database.DatabaseAccessException;
import org.vaultage.server.database.SessionManager;
import org.vaultage.shared.comm.command.ClientCommandType;
import org.vaultage.shared.comm.command.ServerCommand;

import ch.marcsladek.commons.concurrent.Callable;

public class CommandResponseHandler implements Callable<Void, ServerCommand<?>> {

  private static final Logger LOGGER = LoggerFactory.getLogger(CommandResponseHandler.class);

  private final UserManager userManager;
  private final SessionManager sessionManager;
  private final Delegator delegator;
  private final IOutputHandler outputHandler;

  public CommandResponseHandler(UserManager userManager, SessionManager sessionManager,
      Delegator delegator, IOutputHandler outputHandler) {
    this.userManager = Objects.requireNonNull(userManager);
    this.sessionManager = Objects.requireNonNull(sessionManager);
    this.delegator = Objects.requireNonNull(delegator);
    this.outputHandler = Objects.requireNonNull(outputHandler);
  }

  @Override
  public Void call(ServerCommand<?> command) {
    try {
      ClientIdentity identity = userManager.getIdentity(command.getSender());
      LOGGER.trace("New session for command '" + command + "'");
      Session session = sessionManager.newSession(identity);
      try {
        Object response = delegator.delegate(identity, command);
        session.getTransaction().commit();
        LOGGER.trace("Session commit for command '" + command + "'");
        outputHandler.handleResponseForCommand(command, response);
      } catch (Exception exc) {
        session.getTransaction().rollback();
        LOGGER.error("Session rollback for command '" + command + "'", exc);
        // TODO stop stuff (e.g. syncsupervisor) if rollback occured
        outputHandler.handle(command.getSender(), ClientCommandType.ErrorCommand, "TODO");
      }
      sessionManager.closeSession(identity);
    } catch (DatabaseAccessException exc) {
      LOGGER.error("Unable to access database for command '" + command + "'", exc);
    }
    return null;
  }
}
