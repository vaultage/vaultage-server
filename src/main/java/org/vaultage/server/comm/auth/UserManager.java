package org.vaultage.server.comm.auth;

import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.ClientConnectionFactory;
import org.vaultage.server.comm.auth.ClientIdentity.ClientIdentityAuthenticator;
import org.vaultage.shared.comm.command.ServerCommand;

import ch.marcsladek.commons.concurrent.Callable;
import ch.marcsladek.commons.concurrent.Startable;
import ch.marcsladek.jrtnp.clientManager.ClientManager;
import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public class UserManager extends ClientManager implements Startable {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserManager.class);

  private final Map<String, ClientIdentity> identityMap;
  private final Map<String, ClientIdentityAuthenticator> authMap;

  private Authenticator auth;

  protected UserManager(Class<? extends ConnectionFactory> connectionFactoryClass)
      throws ReflectiveOperationException {
    super(connectionFactoryClass);
    identityMap = new ConcurrentHashMap<>();
    authMap = new ConcurrentHashMap<>();
  }

  public void setCallable(Callable<Void, ServerCommand<?>> callable) {
    ((ClientConnectionFactory) factory).setCallable(callable);
  }

  public Authenticator getAuthenticator(String secret) {
    if (auth == null) {
      return auth = new Authenticator(secret, authMap);
    } else {
      throw new IllegalStateException("Authenticator object already generated");
    }
  }

  public ClientIdentity getIdentity(String host) {
    return identityMap.get(host);
  }

  @Override
  protected void newClientNotExistent(Connection newClient) {
    LOGGER.info("New Connection '" + newClient + "'");
    String host = newClient.getIdentifier();
    Entry<ClientIdentity, ClientIdentityAuthenticator> identityEntry = ClientIdentity
        .createClientIdentity(host);
    identityMap.put(host, identityEntry.getKey());
    authMap.put(host, identityEntry.getValue());
    newClient.start();
  }

  @Override
  protected void newClientExistent(Connection newClient) {
    LOGGER.warn("Connection '" + newClient + "' already exists");
  }

  @Override
  protected void newClientIOException(Socket socket, IOException exp) {
    LOGGER.error("Connecting to '" + socket + "' failed", exp);
  }

  @Override
  protected void removed(Connection client) {
    LOGGER.info("Connection '" + client + "' removed and shutted down");
    client.shutdown();
  }

  @Override
  public void start() throws Exception {
  }

  @Override
  public boolean isRunning() {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean isTerminated() {
    throw new UnsupportedOperationException();
  }

}
