package org.vaultage.server.comm.auth;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.ClientIdentity.ClientIdentityAuthenticator;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.classes.User;
import org.vaultage.shared.comm.auth.Auth;
import org.vaultage.shared.comm.auth.AuthChallenge;
import org.vaultage.shared.comm.auth.AuthRequest;
import org.vaultage.shared.comm.auth.AuthResponse;
import org.vaultage.shared.comm.auth.CodecUtils;

public final class Authenticator {

  private static final Logger LOGGER = LoggerFactory.getLogger(Authenticator.class);

  private final String secret;
  private final Map<String, AuthCache> cacheMap;
  private final Map<String, ClientIdentityAuthenticator> authMap;

  Authenticator(String secret, Map<String, ClientIdentityAuthenticator> authMap) {
    this.secret = secret;
    cacheMap = new ConcurrentHashMap<>();
    this.authMap = authMap;
  }

  public AuthChallenge challenge(ClientIdentity identity, AuthRequest authRequest, String host)
      throws DatabaseException {
    String name = authRequest.getUsername();
    String challenge = CodecUtils.generateRandomString(128);
    String salt = getSalt(identity, name, host, challenge);
    LOGGER.info("AuthChallenge for '" + host + "' and user '" + name + "' successfully generated");
    return new AuthChallenge(salt, challenge);
  }

  private String getSalt(ClientIdentity identity, String name, String host, String challenge)
      throws DatabaseException {
    Session session = DatabaseAccess.getSession(identity);
    User user = DatabaseAccess.getReaderProvider().getUserReader().getUser(session, name);
    if (user != null) {
      cacheMap.put(host, getAuthCache(user, challenge));
      return user.getSalt();
    } else {
      LOGGER.info("Host '" + host + "' tried to connect as inexistent user '" + name + "'");
      return CodecUtils.generateFakedSalt(secret, name);
    }
  }

  public AuthResponse authenticate(Auth auth, String host) {
    AuthResponse response = AuthResponse.FAILURE;
    AuthCache authCache = cacheMap.remove(host);
    if (authCache == null) {
      LOGGER.info("Username was wrong for host '" + host + "'");
    } else if (!authCache.validateHash(auth.getHash())) {
      LOGGER.info("Invalid credentials submitted from '" + host + "' for " + auth + "'");
    } else if (!authenticate(host, authCache.getUserId())) {
      LOGGER.warn("Unable to authenticate host '" + host + "' with userId '"
          + authCache.getUserId() + "', already authenticated");
    } else {
      response = AuthResponse.SUCCESS;
    }
    LOGGER.info("Authentification for '" + host + "': " + response);
    return response;
  }

  private boolean authenticate(String host, Long userId) {
    ClientIdentityAuthenticator idenityAuthenticator = authMap.remove(host);
    if (idenityAuthenticator != null) {
      return idenityAuthenticator.authenticate(userId);
    }
    return false;
  }

  private AuthCache getAuthCache(User user, String challenge) {
    return new AuthCache(user.getId(), CodecUtils.generateChallengeHash(user.getHash(), challenge));
  }

  private class AuthCache {

    private final Long userId;
    private final String expectedHash;

    private AuthCache(Long userId, String expectedHash) {
      this.userId = userId;
      this.expectedHash = expectedHash;
    }

    private Long getUserId() {
      return userId;
    }

    private boolean validateHash(String hash) {
      return expectedHash.equals(hash);
    }

  }

  // for test purposes only
  boolean cacheMapOnlyContains(String host) {
    return (cacheMap.size() == 1) && (cacheMap.get(host) != null);
  }

}
