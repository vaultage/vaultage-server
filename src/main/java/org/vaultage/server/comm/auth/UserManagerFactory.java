package org.vaultage.server.comm.auth;

import ch.marcsladek.jrtnp.clientManager.ClientManager;
import ch.marcsladek.jrtnp.clientManager.ClientManagerFactory;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public class UserManagerFactory implements ClientManagerFactory {

  @Override
  public ClientManager newInstance(Class<? extends ConnectionFactory> connectionFactoryClass)
      throws ReflectiveOperationException {
    return new UserManager(connectionFactoryClass);
  }

}
