package org.vaultage.server.comm.auth;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.Objects;

public class ClientIdentity {

  private final String host;
  private Long userId;
  private final ClientIdentityAuthenticator authenticator;

  private ClientIdentity(String host) {
    this.host = host;
    authenticator = new ClientIdentityAuthenticator();
  }

  static final Entry<ClientIdentity, ClientIdentityAuthenticator> createClientIdentity(String host) {
    ClientIdentity identity = new ClientIdentity(host);
    return new SimpleEntry<>(identity, identity.authenticator);
  }

  public String getHost() {
    return host;
  }

  public Long getUserId() {
    return userId;
  }

  public boolean isAuthenticated() {
    return this.userId != null;
  }

  @Override
  public int hashCode() {
    return Objects.hash(host, userId);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof ClientIdentity) {
      ClientIdentity other = (ClientIdentity) obj;
      return Objects.equals(host, other.host) && Objects.equals(userId, other.userId);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "ClientIdentity [host=" + host + ", userId=" + userId + "]";
  }

  class ClientIdentityAuthenticator {

    private ClientIdentityAuthenticator() {
    }

    boolean authenticate(Long userId) {
      if (ClientIdentity.this.userId == null) {
        ClientIdentity.this.userId = userId;
        return true;
      } else {
        return false;
      }
    }

  }

}
