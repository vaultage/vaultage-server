package org.vaultage.server.comm;

import java.io.IOException;
import java.net.Socket;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.shared.comm.command.ServerCommand;

import ch.marcsladek.commons.concurrent.Callable;
import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionException;

public final class ClientConnection extends Connection {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClientConnection.class);

  private final Callable<Void, ServerCommand<?>> callable;

  ClientConnection(Socket socket, Callable<Void, ServerCommand<?>> callable) throws IOException {
    super(Objects.requireNonNull(socket));
    this.callable = Objects.requireNonNull(callable);
  }

  @Override
  protected void process(Object obj) {
    if (obj instanceof ServerCommand) {
      ServerCommand<?> command = (ServerCommand<?>) obj;
      if (command.isValid(getIdentifier(), getLocalAddress())) {
        callable.call(command);
      } else {
        LOGGER.warn("Unable to validate command from '" + getIdentifier() + "': " + command);
      }
    } else {
      LOGGER.warn("Illegal object received from '" + getIdentifier() + "': " + obj);
    }
  }

  @Override
  protected void finish(boolean closedRemotely, ConnectionException closeException) {
    // TODO finish processing
    LOGGER.error("ClientConnection.finish(): implementation needed");
    LOGGER.trace("Closed Connection '" + getIdentifier() + "' "
        + (closedRemotely ? "remotely" : ""), closeException);
  }

}
