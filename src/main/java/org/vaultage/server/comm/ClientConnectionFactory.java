package org.vaultage.server.comm;

import java.io.IOException;
import java.net.Socket;

import org.vaultage.shared.comm.command.ServerCommand;

import ch.marcsladek.commons.concurrent.Callable;
import ch.marcsladek.jrtnp.connection.Connection;
import ch.marcsladek.jrtnp.connection.ConnectionFactory;

public final class ClientConnectionFactory implements ConnectionFactory {

  private Callable<Void, ServerCommand<?>> callable;

  public void setCallable(Callable<Void, ServerCommand<?>> callable) {
    this.callable = callable;
  }

  @Override
  public Connection newInstance(Socket socket) throws IOException {
    return new ClientConnection(socket, callable);
  }

}
