package org.vaultage.server.comm;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.comm.auth.UserManager;
import org.vaultage.server.comm.auth.UserManagerFactory;

import ch.marcsladek.commons.concurrent.Startable;
import ch.marcsladek.jrtnp.server.Server;

public class ConnectionHandler extends Server implements Startable {

  private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionHandler.class);

  public ConnectionHandler(int port, int timeout) throws ReflectiveOperationException, IOException {
    super(port, timeout, false, ClientConnectionFactory.class, UserManagerFactory.class);
  }

  public UserManager getUserManager() {
    return (UserManager) clientManager;
  }

  @Override
  public void started() {
    LOGGER.info("ConnectionHandler started");
  }

  @Override
  public void shuttedDown() {
    LOGGER.info("ConnectionHandler shutdown");
  }

  @Override
  public boolean isRunning() {
    return serverSocketListener.isListening();
  }

  @Override
  public boolean isTerminated() {
    return !serverSocketListener.isListening();
  }

}
