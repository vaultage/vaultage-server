package org.vaultage.server.database.writers;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.same;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.classes.Data;
import org.vaultage.server.database.classes.Meta;
import org.vaultage.server.database.readers.DataReader;
import org.vaultage.server.database.readers.DatabaseReaderProvider;
import org.vaultage.server.database.readers.IDatabaseReaderProvider;

public class DataWriterTest {

  private static DataWriter dataWriter;

  private IDatabaseReaderProvider readerProviderMock;
  private IDatabaseWriterProvider writerProviderMock;
  private DataReader dataReaderMock;
  private MetaWriter metaWriterMock;
  private static Session sessionMock;
  private static Query queryMock;

  @Before
  public void setUp_MetaContainerReaderTest() throws Exception {
    readerProviderMock = createMock(DatabaseReaderProvider.class);
    writerProviderMock = createMock(DatabaseWriterProvider.class);
    DatabaseAccess.initialise(null, readerProviderMock, writerProviderMock, null);
    dataReaderMock = createMock(DataReader.class);
    metaWriterMock = createMock(MetaWriter.class);
    sessionMock = createMock(Session.class);
    queryMock = createMock(Query.class);
    dataWriter = new DataWriter();

    expect(readerProviderMock.getDataReader()).andReturn(dataReaderMock).anyTimes();
    expect(writerProviderMock.getMetaWriter()).andReturn(metaWriterMock).anyTimes();
  }

  @Test
  public void test_writeData() throws DatabaseException {
    Meta meta = new Meta("somePath", new Date(), true);
    String hash = "someHash";
    byte[] dataArray = new byte[] { 0, 1, 2, 3 };
    Data data = new Data(hash, null);
    Long dataId = 3L;

    expect(dataReaderMock.getDataForHash(same(sessionMock), eq(hash))).andReturn(null).once();
    expect(sessionMock.save(eq(data))).andReturn(dataId).once();
    metaWriterMock.update(same(sessionMock), same(meta));
    expectLastCall().once();

    replayAll();
    assertEquals(dataId, dataWriter.writeData(sessionMock, meta, hash, dataArray));
    assertEquals(data, meta.getData());
    verifyAll();
  }

  private void replayAll(Object... mocks) {
    replay(readerProviderMock, writerProviderMock, dataReaderMock, metaWriterMock, sessionMock,
        queryMock);
    replay(mocks);
  }

  private void verifyAll(Object... mocks) {
    verify(readerProviderMock, writerProviderMock, dataReaderMock, metaWriterMock, sessionMock,
        queryMock);
    verify(mocks);
  }

}
