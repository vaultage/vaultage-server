package org.vaultage.server.database.writers;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.marcsladek.commons.hibernate.Field;

public class AbstractWriterTest {

  private static TestWriter writer;

  private static Session sessionMock;
  private static Query queryMock;

  @Before
  public void setUp_AbstractWriterTest() throws Exception {
    sessionMock = createMock(Session.class);
    queryMock = createMock(Query.class);
    writer = new TestWriter();
  }

  @Test
  public void test_save() throws DatabaseWriteException {
    TestClass entity = new TestClass();
    Long entityId = 4L;

    expect(sessionMock.save(same(entity))).andReturn(entityId).once();

    replayAll();
    assertEquals(entityId, writer.save(sessionMock, entity));
    verifyAll();
  }

  @Test
  public void test_save_HibernateException() throws DatabaseWriteException {
    TestClass entity = new TestClass();
    HibernateException cause = new HibernateException("");

    expect(sessionMock.save(same(entity))).andThrow(cause).once();

    replayAll();
    try {
      writer.save(sessionMock, entity);
      fail("Should throw DatabaseWriteException");
    } catch (DatabaseWriteException exc) {
      assertSame(cause, exc.getCause());
    }
    verifyAll();
  }

  @Test
  public void test_save_null() throws DatabaseWriteException {
    replayAll();
    try {
      writer.save(sessionMock, null);
      fail("Should throw NPE");
    } catch (NullPointerException exc) {
      // expected
    }
    verifyAll();
  }

  @Test
  public void test_update() throws DatabaseWriteException {
    TestClass entity = new TestClass();

    sessionMock.update(same(entity));
    expectLastCall().once();

    replayAll();
    writer.update(sessionMock, entity);
    verifyAll();
  }

  @Test
  public void test_update_HibernateException() throws DatabaseWriteException {
    TestClass entity = new TestClass();
    HibernateException cause = new HibernateException("");

    sessionMock.update(same(entity));
    expectLastCall().andThrow(cause).once();

    replayAll();
    try {
      writer.update(sessionMock, entity);
      fail("Should throw DatabaseWriteException");
    } catch (DatabaseWriteException exc) {
      assertSame(cause, exc.getCause());
    }
    verifyAll();
  }

  @Test
  public void test_update_null() throws DatabaseWriteException {
    replayAll();
    try {
      writer.update(sessionMock, null);
      fail("Should throw NPE");
    } catch (NullPointerException exc) {
      // expected
    }
    verifyAll();
  }

  private void replayAll(Object... mocks) {
    replay(sessionMock, queryMock);
    replay(mocks);
  }

  private void verifyAll(Object... mocks) {
    verify(sessionMock, queryMock);
    verify(mocks);
  }

  private class TestClass {
  }

  enum TestField implements Field {
    asdf, fdsa;
  }

  private class TestWriter extends AbstractWriter<TestClass> {

    TestWriter() {
      super(TestClass.class);
    }

    @Override
    Logger getLogger() {
      return LoggerFactory.getLogger(TestWriter.class);
    }

  }

}
