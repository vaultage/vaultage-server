package org.vaultage.server.database.readers;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.server.database.DatabaseException;

import ch.marcsladek.commons.hibernate.Field;
import ch.marcsladek.commons.hibernate.query.BindedDatabaseQuery;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryBuilder;
import ch.marcsladek.commons.hibernate.query.DatabaseQueryType;

public class AbstractReaderTest {

  private static TestReader reader;

  private static Session sessionMock;
  private static Query queryMock;

  @Before
  public void setUp_AbstractReaderTest() throws Exception {
    sessionMock = createMock(Session.class);
    queryMock = createMock(Query.class);
    reader = new TestReader();
  }

  @Test
  public void test_getUserCount() throws DatabaseException {
    Long count = 3L;

    String hql = "SELECT COUNT(id) FROM TestClass";
    expect(sessionMock.createQuery(eq(hql))).andReturn(queryMock).once();
    expect(queryMock.uniqueResult()).andReturn(count).once();

    replayAll();
    assertEquals(count, reader.count(sessionMock));
    verifyAll();
  }

  @Test
  public void test_getUserCount_HibernateException() {
    Throwable cause = new HibernateException("asdf");

    String hql = "SELECT COUNT(id) FROM TestClass";
    expect(sessionMock.createQuery(eq(hql))).andReturn(queryMock).once();
    expect(queryMock.uniqueResult()).andThrow(cause);

    replayAll();
    try {
      reader.count(sessionMock);
      fail("Should throw DatabaseException");
    } catch (DatabaseException exc) {
      assertEquals(cause, exc.getCause());
    }
    verifyAll();
  }

  @Test
  public void test_getUser_userId() throws DatabaseException {
    Long id = 5L;
    TestClass testClass = new TestClass();

    expect(sessionMock.get(same(TestClass.class), eq(id))).andReturn(testClass).once();

    replayAll();
    assertEquals(testClass, reader.getForId(sessionMock, id));
    verifyAll();
  }

  @Test
  public void test_getUser_userId_null() throws DatabaseException {
    Long id = 5L;

    expect(sessionMock.get(same(TestClass.class), eq(id))).andReturn(null).once();

    replayAll();
    assertNull(reader.getForId(sessionMock, id));
    verifyAll();
  }

  @Test
  public void test_get() throws DatabaseReadException {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestClass.class);
    builder.addField(TestField.asdf);
    builder.addField(TestField.fdsa);
    Map<Field, Object> binds = new HashMap<>();
    binds.put(TestField.asdf, true);
    binds.put(TestField.fdsa, new Date(1000));
    BindedDatabaseQuery dbQuery = builder.build().getBindedDatabaseQuery(binds);
    List<TestClass> retList = Arrays.asList(new TestClass[] { new TestClass(), new TestClass() });

    expect(sessionMock.createQuery(eq(dbQuery.getQueryString()))).andReturn(queryMock).once();
    expect(queryMock.setBoolean(eq("asdf"), eq(true))).andReturn(queryMock).once();
    expect(queryMock.setDate(eq("fdsa"), eq(new Date(1000)))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    assertSame(retList, reader.get(sessionMock, dbQuery));
    verifyAll();
  }

  @Test
  public void test_get_HibernateException() throws DatabaseReadException {
    Throwable cause = new HibernateException("asdf");
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestClass.class);
    builder.addField(TestField.asdf);
    builder.addField(TestField.fdsa);
    builder.setLimit(5);
    Map<Field, Object> binds = new HashMap<>();
    binds.put(TestField.asdf, "test1");
    binds.put(TestField.fdsa, 7L);
    BindedDatabaseQuery dbQuery = builder.build().getBindedDatabaseQuery(binds);

    expect(sessionMock.createQuery(eq(dbQuery.getQueryString()))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("asdf"), eq("test1"))).andReturn(queryMock).once();
    expect(queryMock.setLong(eq("fdsa"), eq(7L))).andReturn(queryMock).once();
    expect(queryMock.setMaxResults(eq(5))).andReturn(queryMock).once();
    expect(queryMock.list()).andThrow(cause).once();

    replayAll();
    try {
      reader.get(sessionMock, dbQuery);
      fail("Should throw DatabaseException");
    } catch (DatabaseException exc) {
      assertEquals(cause, exc.getCause());
    }
    verifyAll();
  }

  @Test
  public void test_getUnique() throws DatabaseReadException {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestClass.class);
    builder.addField(TestField.asdf);
    builder.addField(TestField.fdsa);
    Map<Field, Object> binds = new HashMap<>();
    binds.put(TestField.asdf, "test1");
    binds.put(TestField.fdsa, "test2");
    BindedDatabaseQuery dbQuery = builder.build().getBindedDatabaseQuery(binds);
    List<TestClass> retList = Arrays.asList(new TestClass[] { new TestClass() });

    expect(sessionMock.createQuery(eq(dbQuery.getQueryString()))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("asdf"), eq("test1"))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("fdsa"), eq("test2"))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    assertSame(retList.get(0), reader.getUnique(sessionMock, dbQuery));
    verifyAll();
  }

  @Test
  public void test_getUnique_empty() throws DatabaseReadException {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestClass.class);
    builder.addField(TestField.asdf);
    builder.addField(TestField.fdsa);
    Map<Field, Object> binds = new HashMap<>();
    binds.put(TestField.asdf, "test1");
    binds.put(TestField.fdsa, "test2");
    BindedDatabaseQuery dbQuery = builder.build().getBindedDatabaseQuery(binds);
    List<TestClass> retList = Arrays.asList(new TestClass[] {});

    expect(sessionMock.createQuery(eq(dbQuery.getQueryString()))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("asdf"), eq("test1"))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("fdsa"), eq("test2"))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    assertNull(reader.getUnique(sessionMock, dbQuery));
    verifyAll();
  }

  @Test
  public void test_getUnique_tooMany() throws DatabaseReadException {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestClass.class);
    builder.addField(TestField.asdf);
    builder.addField(TestField.fdsa);
    Map<Field, Object> binds = new HashMap<>();
    binds.put(TestField.asdf, 5);
    binds.put(TestField.fdsa, "test2");
    BindedDatabaseQuery dbQuery = builder.build().getBindedDatabaseQuery(binds);
    List<TestClass> retList = Arrays.asList(new TestClass[] { new TestClass(), new TestClass() });

    expect(sessionMock.createQuery(eq(dbQuery.getQueryString()))).andReturn(queryMock).once();
    expect(queryMock.setInteger(eq("asdf"), eq(5))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("fdsa"), eq("test2"))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    try {
      reader.getUnique(sessionMock, dbQuery);
      fail("Should throw DatabaseException");
    } catch (DatabaseException exc) {
      assertNull(exc.getCause());
    }
    verifyAll();
  }

  private void replayAll(Object... mocks) {
    replay(sessionMock, queryMock);
    replay(mocks);
  }

  private void verifyAll(Object... mocks) {
    verify(sessionMock, queryMock);
    verify(mocks);
  }

  private class TestClass {
  }

  enum TestField implements Field {
    asdf, fdsa;
  }

  private class TestReader extends AbstractReader<TestClass> {

    TestReader() {
      super(TestClass.class);
    }

    @Override
    Logger getLogger() {
      return LoggerFactory.getLogger(TestReader.class);
    }

  }

}
