package org.vaultage.server.database.readers;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.same;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.classes.Meta;
import org.vaultage.server.database.classes.MetaContainer;
import org.vaultage.server.database.classes.User;

public class MetaContainerReaderTest {

  private static MetaContainerReader metaContainerReader;

  private IDatabaseReaderProvider readerProviderMock;
  private UserReader userReaderMock;
  private static Session sessionMock;
  private static Query queryMock;

  @Before
  public void setUp_MetaContainerReaderTest() throws Exception {
    readerProviderMock = createMock(DatabaseReaderProvider.class);
    userReaderMock = createMock(UserReader.class);
    DatabaseAccess.initialise(null, readerProviderMock, null, null);
    sessionMock = createMock(Session.class);
    queryMock = createMock(Query.class);
    metaContainerReader = new MetaContainerReader();

    expect(readerProviderMock.getUserReader()).andReturn(userReaderMock).anyTimes();
  }

  @Test
  public void test_getLastMetaContainer() throws DatabaseReadException {
    Long userId = 5L;
    User user = new User("someName", "hash", "salt");
    MetaContainer metaContainer = new MetaContainer(user, "/", new Date(), new HashSet<Meta>());
    List<MetaContainer> retList = Arrays.asList(new MetaContainer[] { metaContainer });

    expect(userReaderMock.getForId(same(sessionMock), eq(userId))).andReturn(user).once();
    String hql = "FROM MetaContainer WHERE user = :user AND status = :status ORDER BY date DESC";
    expect(sessionMock.createQuery(eq(hql))).andReturn(queryMock).once();
    expect(queryMock.setEntity(eq("user"), eq(user))).andReturn(queryMock).once();
    expect(queryMock.setInteger(eq("status"), eq(1))).andReturn(queryMock).once();
    expect(queryMock.setMaxResults(eq(1))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    assertSame(retList.get(0), metaContainerReader.getLastMetaContainer(sessionMock, userId));
    verifyAll();
  }

  @Test
  public void test_getLastMetaContainer_nouser() throws DatabaseReadException {
    Long userId = 5L;

    expect(userReaderMock.getForId(same(sessionMock), eq(userId))).andReturn(null).once();

    replay(readerProviderMock, userReaderMock);
    try {
      metaContainerReader.getLastMetaContainer(sessionMock, userId);
      fail("expecting DatabaseException");
    } catch (DatabaseReadException exc) {
      // expected
    }
    verify(readerProviderMock, userReaderMock);
  }

  @Test
  public void test_getLastMetaContainer_null() throws DatabaseReadException {
    try {
      metaContainerReader.getLastMetaContainer(sessionMock, null);
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
  }

  private void replayAll(Object... mocks) {
    replay(readerProviderMock, userReaderMock, sessionMock, queryMock);
    replay(mocks);
  }

  private void verifyAll(Object... mocks) {
    verify(readerProviderMock, userReaderMock, sessionMock, queryMock);
    verify(mocks);
  }

}
