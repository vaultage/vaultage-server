package org.vaultage.server.database.readers;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.server.database.classes.Data;

public class DataReaderTest {

  private static DataReader dataReader;

  private static Session sessionMock;
  private static Query queryMock;

  @Before
  public void setUp_DataReaderTest() throws Exception {
    sessionMock = createMock(Session.class);
    queryMock = createMock(Query.class);
    dataReader = new DataReader();
  }

  @Test
  public void test_getDataForHash() throws DatabaseReadException {
    String hash = "someHash";
    Data data = new Data(hash, null);
    List<Data> retList = Arrays.asList(new Data[] { data });

    String hql = "FROM Data WHERE hash = :hash";
    expect(sessionMock.createQuery(eq(hql))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("hash"), eq(hash))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    assertSame(retList.get(0), dataReader.getDataForHash(sessionMock, hash));
    verifyAll();
  }

  @Test
  public void test_getDataForHash_tooMany() throws DatabaseReadException {
    String hash = "someHash";
    Data data = new Data(hash, null);
    List<Data> retList = Arrays.asList(new Data[] { data, data });

    String hql = "FROM Data WHERE hash = :hash";
    expect(sessionMock.createQuery(eq(hql))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("hash"), eq(hash))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    try {
      dataReader.getDataForHash(sessionMock, hash);
      fail("expecting DatabaseReadException");
    } catch (DatabaseReadException exc) {
      // expected
    }
    verifyAll();
  }

  @Test
  public void test_getDataForHash_null() throws DatabaseReadException {
    try {
      dataReader.getDataForHash(sessionMock, null);
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
  }

  private void replayAll(Object... mocks) {
    replay(sessionMock, queryMock);
    replay(mocks);
  }

  private void verifyAll(Object... mocks) {
    verify(sessionMock, queryMock);
    verify(mocks);
  }

}
