package org.vaultage.server.database.readers;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.server.database.classes.Data;
import org.vaultage.server.database.classes.Meta;

public class MetaReaderTest {

  private static MetaReader metaReader;

  private static Session sessionMock;
  private static Query queryMock;

  @Before
  public void setUp_MetaReaderTest() throws Exception {
    sessionMock = createMock(Session.class);
    queryMock = createMock(Query.class);
    metaReader = new MetaReader();
  }

  @Test
  public void test_getMetaIsNotFile() throws DatabaseReadException {
    String path = "somePath";
    Date changeDate = new Date();
    Meta meta = new Meta(path, changeDate, false);
    List<Meta> retList = Arrays.asList(new Meta[] { meta });

    String hql = "FROM Meta WHERE path = :path AND changeDate = :changeDate AND isFile = :isFile";
    expect(sessionMock.createQuery(eq(hql))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("path"), eq(path))).andReturn(queryMock).once();
    expect(queryMock.setDate(eq("changeDate"), eq(changeDate))).andReturn(queryMock).once();
    expect(queryMock.setBoolean(eq("isFile"), eq(false))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    assertSame(retList.get(0), metaReader.getMetaIsNotFile(sessionMock, path, changeDate));
    verifyAll();
  }

  @Test
  public void test_getMetaIsNotFile_null() throws DatabaseReadException {
    try {
      metaReader.getMetaIsNotFile(sessionMock, "", null);
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
    try {
      metaReader.getMetaIsNotFile(sessionMock, null, new Date());
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
  }

  @Test
  public void test_getMetaIsFile() throws DatabaseReadException {
    String path = "somePath";
    Date changeDate = new Date();
    Data data = new Data();
    Meta meta = new Meta(path, changeDate, true);
    meta.setData(data);
    List<Meta> retList = Arrays.asList(new Meta[] { meta });

    String hql = "FROM Meta WHERE path = :path AND changeDate = :changeDate AND isFile = :isFile "
        + "AND data = :data";
    expect(sessionMock.createQuery(eq(hql))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("path"), eq(path))).andReturn(queryMock).once();
    expect(queryMock.setDate(eq("changeDate"), eq(changeDate))).andReturn(queryMock).once();
    expect(queryMock.setBoolean(eq("isFile"), eq(true))).andReturn(queryMock).once();
    expect(queryMock.setEntity(eq("data"), eq(data))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    assertSame(retList.get(0), metaReader.getMetaIsFile(sessionMock, path, changeDate, data));
    verifyAll();
  }

  @Test
  public void test_getMetaIsFile_null() throws DatabaseReadException {
    try {
      metaReader.getMetaIsFile(sessionMock, "", new Date(), null);
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
    try {
      metaReader.getMetaIsFile(sessionMock, "", null, new Data());
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
    try {
      metaReader.getMetaIsFile(sessionMock, null, new Date(), new Data());
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
  }

  private void replayAll(Object... mocks) {
    replay(sessionMock, queryMock);
    replay(mocks);
  }

  private void verifyAll(Object... mocks) {
    verify(sessionMock, queryMock);
    verify(mocks);
  }

}
