package org.vaultage.server.database.readers;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.server.database.classes.User;

public class UserReaderTest {

  private static UserReader reader;

  private static Session sessionMock;
  private static Query queryMock;

  @Before
  public void setUp_UserReaderTest() throws Exception {
    sessionMock = createMock(Session.class);
    queryMock = createMock(Query.class);
    reader = new UserReader();
  }

  @Test
  public void test_getUser_name() throws DatabaseReadException {
    String name = "someName";
    List<User> retList = Arrays.asList(new User[] { new User(name, "hash", "salt") });

    expect(sessionMock.createQuery(eq("FROM User WHERE name = :name"))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("name"), eq(name))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    assertSame(retList.get(0), reader.getUser(sessionMock, name));
    verifyAll();
  }

  @Test
  public void test_getUser_name_null() throws DatabaseReadException {
    try {
      reader.getUser(sessionMock, null);
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
  }

  @Test
  public void test_getUser_name_hash() throws DatabaseReadException {
    String name = "someName";
    String hash = "someHash";
    List<User> retList = Arrays.asList(new User[] { new User(name, hash, "salt") });

    expect(sessionMock.createQuery(eq("FROM User WHERE name = :name AND hash = :hash"))).andReturn(
        queryMock).once();
    expect(queryMock.setString(eq("name"), eq(name))).andReturn(queryMock).once();
    expect(queryMock.setString(eq("hash"), eq(hash))).andReturn(queryMock).once();
    expect(queryMock.list()).andReturn(retList).once();

    replayAll();
    assertSame(retList.get(0), reader.getUser(sessionMock, name, hash));
    verifyAll();
  }

  @Test
  public void test_getUser_name_hash_null() throws DatabaseReadException {
    try {
      reader.getUser(sessionMock, null, "someHash");
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
    try {
      reader.getUser(sessionMock, "someName", null);
      fail("expecting NPE");
    } catch (NullPointerException exc) {
      // expected
    }
  }

  private void replayAll(Object... mocks) {
    replay(sessionMock, queryMock);
    replay(mocks);
  }

  private void verifyAll(Object... mocks) {
    verify(sessionMock, queryMock);
    verify(mocks);
  }

}
