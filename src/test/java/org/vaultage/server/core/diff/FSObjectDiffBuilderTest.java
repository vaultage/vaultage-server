package org.vaultage.server.core.diff;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.meta.IMeta;
import org.vaultage.shared.fs.meta.MetaFile;
import org.vaultage.shared.fs.meta.MetaFolder;

public class FSObjectDiffBuilderTest {

  private FSObjectDiffBuilder builder;

  private final Path TEST = Paths.get("test");
  private final Path DIR1 = TEST.resolve("dir1");
  private final Path DIR2 = TEST.resolve("dir2");
  private final Path DIR1_DIR3 = DIR1.resolve("dir3");
  private final Path DIR1_DIR3_FILE4 = DIR1_DIR3.resolve("file4");
  private final Path DIR1_FILE2 = DIR1.resolve("file2");
  private final Path DIR2_FILE3 = DIR2.resolve("file3");
  private final Path DIR1_FILE1 = DIR1.resolve("file1");

  private final Path DIR2_FILENEW = DIR2.resolve("fileNew");
  private final Path DIR2_DIRNEW = DIR2.resolve("dirNew");

  private final String CONTENT_FILE1 = "This is file1";
  private final String CONTENT_FILE2 = "This is file2";
  private final String CONTENT_FILE3 = "This is file3";
  private final String CONTENT_FILE4 = "This is file4";
  private final String CONTENT_DIR3 = "This is no longer a dir3";
  private final String CONTENT_FILENEW = "This is a new file";
  private final String CONTENT_FILEMOD = "This is file1 but with new content";

  @Before
  public void setUp() throws IOException {
    builder = new FSObjectDiffBuilder();
  }

  // TODO test errors (e.g. from DiffFactory)

  @Test
  public void test_build_equal() throws Exception {
    FSObject<IMeta> fsObjOld = getFullFSObjectMeta();
    FSObject<IMeta> fsObjNew = getFullFSObjectMeta();

    FSObject<Diff> fsObjDiff = builder.build(fsObjOld, fsObjNew);
    assertNotNull(fsObjDiff);
    assertEquals("test", fsObjDiff.getRoot());
    Map<String, Diff> diffMap = fsObjDiff.getClonedMap();
    assertDiffFolder(diffMap.remove(""), DiffType.UNCHANGED, "");
    assertDiffFolder(diffMap.remove("dir1"), DiffType.UNCHANGED, "dir1");
    assertDiffFolder(diffMap.remove("dir2"), DiffType.UNCHANGED, "dir2");
    assertDiffFolder(diffMap.remove("dir1/dir3"), DiffType.UNCHANGED, "dir1/dir3");
    assertDiffFile(diffMap.remove("dir1/file1"), DiffType.UNCHANGED, "dir1/file1",
        DigestUtils.sha1Hex(CONTENT_FILE1));
    assertDiffFile(diffMap.remove("dir1/file2"), DiffType.UNCHANGED, "dir1/file2",
        DigestUtils.sha1Hex(CONTENT_FILE2));
    assertDiffFile(diffMap.remove("dir2/file3"), DiffType.UNCHANGED, "dir2/file3",
        DigestUtils.sha1Hex(CONTENT_FILE3));
    assertDiffFile(diffMap.remove("dir1/dir3/file4"), DiffType.UNCHANGED, "dir1/dir3/file4",
        DigestUtils.sha1Hex(CONTENT_FILE4));
    assertEquals(0, diffMap.size());
  }

  @Test
  public void test_build_allDeleted() throws Exception {
    FSObject<IMeta> fsObjOld = getFullFSObjectMeta();
    FSObject<IMeta> fsObjNew = getEmptyFSObjectMeta();

    FSObject<Diff> fsObjDiff = builder.build(fsObjOld, fsObjNew);
    assertNotNull(fsObjDiff);
    assertEquals("test", fsObjDiff.getRoot());
    Map<String, Diff> diffMap = fsObjDiff.getClonedMap();
    assertDiffFolder(diffMap.remove(""), DiffType.UNCHANGED, "");
    assertDiffFolder(diffMap.remove("dir1"), DiffType.DELETED, "dir1");
    assertDiffFolder(diffMap.remove("dir2"), DiffType.DELETED, "dir2");
    assertDiffFolder(diffMap.remove("dir1/dir3"), DiffType.DELETED, "dir1/dir3");
    assertDiffFile(diffMap.remove("dir1/file1"), DiffType.DELETED, "dir1/file1",
        DigestUtils.sha1Hex(CONTENT_FILE1));
    assertDiffFile(diffMap.remove("dir1/file2"), DiffType.DELETED, "dir1/file2",
        DigestUtils.sha1Hex(CONTENT_FILE2));
    assertDiffFile(diffMap.remove("dir2/file3"), DiffType.DELETED, "dir2/file3",
        DigestUtils.sha1Hex(CONTENT_FILE3));
    assertDiffFile(diffMap.remove("dir1/dir3/file4"), DiffType.DELETED, "dir1/dir3/file4",
        DigestUtils.sha1Hex(CONTENT_FILE4));
    assertEquals(0, diffMap.size());
  }

  @Test
  public void test_build_allCreated() throws Exception {
    FSObject<IMeta> fsObjOld = getEmptyFSObjectMeta();
    FSObject<IMeta> fsObjNew = getFullFSObjectMeta();

    FSObject<Diff> fsObjDiff = builder.build(fsObjOld, fsObjNew);
    assertNotNull(fsObjDiff);
    assertEquals("test", fsObjDiff.getRoot());
    Map<String, Diff> diffMap = fsObjDiff.getClonedMap();
    assertDiffFolder(diffMap.remove(""), DiffType.UNCHANGED, "");
    assertDiffFolder(diffMap.remove("dir1"), DiffType.CREATED, "dir1");
    assertDiffFolder(diffMap.remove("dir2"), DiffType.CREATED, "dir2");
    assertDiffFolder(diffMap.remove("dir1/dir3"), DiffType.CREATED, "dir1/dir3");
    assertDiffFile(diffMap.remove("dir1/file1"), DiffType.CREATED, "dir1/file1",
        DigestUtils.sha1Hex(CONTENT_FILE1));
    assertDiffFile(diffMap.remove("dir1/file2"), DiffType.CREATED, "dir1/file2",
        DigestUtils.sha1Hex(CONTENT_FILE2));
    assertDiffFile(diffMap.remove("dir2/file3"), DiffType.CREATED, "dir2/file3",
        DigestUtils.sha1Hex(CONTENT_FILE3));
    assertDiffFile(diffMap.remove("dir1/dir3/file4"), DiffType.CREATED, "dir1/dir3/file4",
        DigestUtils.sha1Hex(CONTENT_FILE4));
    assertEquals(0, diffMap.size());
  }

  @Test
  public void test_buildDiff_diff() throws Exception {
    FSObject<IMeta> fsObjOld = getFullFSObjectMeta();
    FSObject<IMeta> fsObjNew = getChangedFSObjectMeta();

    FSObject<Diff> fsObjDiff = builder.build(fsObjOld, fsObjNew);
    assertNotNull(fsObjDiff);
    assertEquals("test", fsObjDiff.getRoot());
    Map<String, Diff> diffMap = fsObjDiff.getClonedMap();
    assertDiffFolder(diffMap.remove(""), DiffType.UNCHANGED, "");
    assertDiffFolder(diffMap.remove("dir1"), DiffType.UNCHANGED, "dir1");
    assertDiffFolder(diffMap.remove("dir2"), DiffType.UNCHANGED, "dir2");
    assertDiffFolderMod(diffMap.remove("dir1/dir3"), "dir1/dir3", DigestUtils.sha1Hex(CONTENT_DIR3));
    assertDiffFileMod(diffMap.remove("dir1/file1"), "dir1/file1",
        DigestUtils.sha1Hex(CONTENT_FILE1), DigestUtils.sha1Hex(CONTENT_FILEMOD));
    assertDiffFile(diffMap.remove("dir1/file2"), DiffType.DELETED, "dir1/file2",
        DigestUtils.sha1Hex(CONTENT_FILE2));
    assertDiffFile(diffMap.remove("dir2/file3"), DiffType.UNCHANGED, "dir2/file3",
        DigestUtils.sha1Hex(CONTENT_FILE3));
    assertDiffFile(diffMap.remove("dir1/dir3/file4"), DiffType.DELETED, "dir1/dir3/file4",
        DigestUtils.sha1Hex(CONTENT_FILE4));
    assertDiffFolder(diffMap.remove("dir2/dirNew"), DiffType.CREATED, "dir2/dirNew");
    assertDiffFile(diffMap.remove("dir2/fileNew"), DiffType.CREATED, "dir2/fileNew",
        DigestUtils.sha1Hex(CONTENT_FILENEW));
    assertEquals(0, diffMap.size());
  }

  @After
  public void breakDown() throws Exception {
    FileUtils.deleteDirectory(TEST.toFile());
  }

  private void assertMetaFolder(IMeta meta, String path) {
    assertNotNull(meta);
    assertEquals(path, meta.getPath());
    assertTrue(meta.isFolder());
    assertNull(meta.getHash());
  }

  private void assertMetaFile(IMeta meta, String path, String hash) {
    assertNotNull(meta);
    assertEquals(path, meta.getPath());
    assertFalse(meta.isFolder());
    assertEquals(hash, meta.getHash());
  }

  private void assertDiffFolder(Diff diff, DiffType type, String path) {
    assertNotNull(diff);
    assertFalse(DiffType.MODIFIED == diff.getDiffType());
    assertEquals(type, diff.getDiffType());
    assertMetaFolder(diff.getMeta(), path);
    assertNull(diff.getMetaOld());
  }

  private void assertDiffFolderMod(Diff diff, String path, String hash) {
    assertNotNull(diff);
    assertEquals(DiffType.MODIFIED, diff.getDiffType());
    assertMetaFile(diff.getMeta(), path, hash);
    assertMetaFolder(diff.getMetaOld(), path);
  }

  private void assertDiffFile(Diff diff, DiffType type, String path, String hash) {
    assertNotNull(diff);
    assertFalse(DiffType.MODIFIED == diff.getDiffType());
    assertEquals(type, diff.getDiffType());
    assertMetaFile(diff.getMeta(), path, hash);
    assertNull(diff.getMetaOld());
  }

  private void assertDiffFileMod(Diff diff, String path, String hashOld, String hashNew) {
    assertNotNull(diff);
    assertEquals(DiffType.MODIFIED, diff.getDiffType());
    assertMetaFile(diff.getMeta(), path, hashNew);
    assertMetaFile(diff.getMetaOld(), path, hashOld);
  }

  private FSObject<IMeta> getEmptyFSObjectMeta() {
    FSObject<IMeta> fsObj = new FSObject<>("test");
    putFolder(fsObj, TEST);
    return fsObj;
  }

  private FSObject<IMeta> getFullFSObjectMeta() {
    FSObject<IMeta> fsObj = getEmptyFSObjectMeta();
    putFolder(fsObj, DIR1);
    putFolder(fsObj, DIR2);
    putFolder(fsObj, DIR1_DIR3);
    putFile(fsObj, DIR1_FILE1, CONTENT_FILE1);
    putFile(fsObj, DIR1_FILE2, CONTENT_FILE2);
    putFile(fsObj, DIR2_FILE3, CONTENT_FILE3);
    putFile(fsObj, DIR1_DIR3_FILE4, CONTENT_FILE4);
    return fsObj;
  }

  private FSObject<IMeta> getChangedFSObjectMeta() {
    FSObject<IMeta> fsObj = getEmptyFSObjectMeta();
    putFolder(fsObj, DIR1);
    putFolder(fsObj, DIR2);
    putFolder(fsObj, DIR2_DIRNEW);
    putFile(fsObj, DIR1_FILE1, CONTENT_FILEMOD);
    putFile(fsObj, DIR2_FILE3, CONTENT_FILE3);
    putFile(fsObj, DIR1_DIR3, CONTENT_DIR3);
    putFile(fsObj, DIR2_FILENEW, CONTENT_FILENEW);
    return fsObj;
  }

  private void putFolder(FSObject<IMeta> fsObj, Path path) {
    String relPath = TEST.relativize(path).toString();
    fsObj.put(relPath, new MetaFolder(relPath, new Date()));
  }

  private void putFile(FSObject<IMeta> fsObj, Path path, String content) {
    String relPath = TEST.relativize(path).toString();
    fsObj.put(relPath, new MetaFile(relPath, new Date(), DigestUtils.sha1Hex(content.getBytes())));
  }

}
