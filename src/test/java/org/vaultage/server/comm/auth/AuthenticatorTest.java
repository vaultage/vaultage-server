package org.vaultage.server.comm.auth;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.same;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.server.comm.auth.ClientIdentity.ClientIdentityAuthenticator;
import org.vaultage.server.database.DatabaseAccess;
import org.vaultage.server.database.DatabaseException;
import org.vaultage.server.database.SessionManager;
import org.vaultage.server.database.classes.User;
import org.vaultage.server.database.readers.DatabaseReadException;
import org.vaultage.server.database.readers.DatabaseReaderProvider;
import org.vaultage.server.database.readers.IDatabaseReaderProvider;
import org.vaultage.server.database.readers.UserReader;
import org.vaultage.shared.comm.auth.AuthChallenge;
import org.vaultage.shared.comm.auth.AuthRequest;

public class AuthenticatorTest {

  private Authenticator authenticator;
  private ClientIdentity identity;

  private IDatabaseReaderProvider readerProviderMock;
  private UserReader userReaderMock;
  private SessionManager sessionManagerMock;
  private Session sessionMock;

  @Before
  public void setUp_AuthenticatorTest() throws Exception {
    readerProviderMock = createMock(DatabaseReaderProvider.class);
    userReaderMock = createMock(UserReader.class);
    sessionManagerMock = createMock(SessionManager.class);
    sessionMock = createMock(Session.class);
    authenticator = new Authenticator("", new HashMap<String, ClientIdentityAuthenticator>());
    identity = ClientIdentity.createClientIdentity("asdf").getKey();
    DatabaseAccess.initialise(sessionManagerMock, readerProviderMock, null, null);

    expect(readerProviderMock.getUserReader()).andReturn(userReaderMock).anyTimes();
    expect(sessionManagerMock.getSession(eq(identity))).andReturn(sessionMock).once();
  }

  @Test
  public void test_challenge() throws DatabaseException {
    String host = "someHost";
    String name = "test";
    String hash = "asdf";
    String salt = "fdsa";
    User user = new User(name, hash, salt);
    AuthRequest authRequest = new AuthRequest(name);

    expect(userReaderMock.getUser(same(sessionMock), eq(name))).andReturn(user).once();

    replayAll();
    AuthChallenge challenge = authenticator.challenge(identity, authRequest, host);
    assertNotNull(challenge);
    assertEquals(salt, challenge.getSalt());
    assertNotNull(challenge.getChallenge());
    assertEquals(128, challenge.getChallenge().length());
    assertTrue(authenticator.cacheMapOnlyContains(host));
    verifyAll();
  }

  @Test
  public void test_challenge_null() throws DatabaseException {
    String host = "someHost";
    String name = "test";
    AuthRequest authRequest = new AuthRequest(name);

    expect(userReaderMock.getUser(same(sessionMock), eq(name))).andReturn(null).once();

    replayAll();
    AuthChallenge challenge = authenticator.challenge(identity, authRequest, host);
    assertNotNull(challenge);
    assertNotNull(challenge.getSalt());
    assertEquals(128, challenge.getSalt().length());
    assertNotNull(challenge.getChallenge());
    assertEquals(128, challenge.getChallenge().length());
    assertFalse(authenticator.cacheMapOnlyContains(host));
    verifyAll();
  }

  @Test
  public void test_challenge_DatabaseException() throws DatabaseException {
    Throwable cause = new DatabaseReadException();
    String host = "someHost";
    String name = "test";
    AuthRequest authRequest = new AuthRequest(name);

    expect(userReaderMock.getUser(same(sessionMock), eq(name))).andThrow(cause).once();

    replayAll();
    try {
      authenticator.challenge(identity, authRequest, host);
      fail("Excpecting DatabaseReadException");
    } catch (DatabaseReadException exc) {
      assertSame(cause, exc);
    }
    verifyAll();
  }

  // TODO tests for authenticate without creating security issues

  private void replayAll(Object... mocks) {
    replay(readerProviderMock, userReaderMock, sessionManagerMock, sessionMock);
    replay(mocks);
  }

  private void verifyAll(Object... mocks) {
    verify(readerProviderMock, userReaderMock, sessionManagerMock, sessionMock);
    verify(mocks);
  }

}
